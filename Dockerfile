FROM node:20.11

RUN apt-get update && apt-get install -y chromium
ENV CHROME_BIN=/usr/bin/chromium
RUN npm install -g @angular/cli@17

WORKDIR /app

# Non-root user to prevent security risks
RUN groupadd -r nonroot \
    && useradd -r -g nonroot -G audio,video nonroot \
    && mkdir -p /home/nonroot \
    && chown -R nonroot:nonroot /home/nonroot \
    && chown -R nonroot:nonroot /app

USER nonroot