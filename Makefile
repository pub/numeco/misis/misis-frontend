# Include environment variables from the .env file
include .env
export

# Default target executed when no arguments are given to make.
default_target: all

.PHONY: default_target all build tag login push

all: build tag login push

build:
	@echo "Building the Docker image..."
	docker build -t $(IMAGE_NAME):$(IMAGE_TAG) .

tag:
	@echo "Tagging the Docker image..."
	docker tag $(IMAGE_NAME):$(IMAGE_TAG) $(REGISTRY_URL)/$(IMAGE_NAME):$(IMAGE_TAG)

login:
	@echo "Logging in to the Docker registry..."
	@docker login $(REGISTRY_URL) -u $(USERNAME) -p $(PASSWORD)

push:
	@echo "Pushing the Docker image to the registry..."
	docker push $(REGISTRY_URL)/$(IMAGE_NAME):$(IMAGE_TAG)

log:
	@echo "REGISTRY_URL URL : $(REGISTRY_URL)"
	@echo "IMAGE : $(IMAGE_NAME):$(IMAGE_TAG)"
	@echo "USERNAME : $(USERNAME)"
