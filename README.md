# MisisFrontend

## Commandes de base

- Démarrer le serveur de développement : `ng serve`.
- Lancer les tests unitaires : `ng test`

## Mise à jour de l'image de build CI

Pour optimiser la pipeline du CI de gitlab, une image custom a été crée pour contenir le CLI d'Angular et une version de chromium pour exécuter les tests unitaires du
projet avec Karma en headless.

Lors d'une montée de version majeure de l'application (ex: Angular v17 > Angular v18), il faut mettre à jour l'application avec la nouvelle version, et mettre à jour la nouvelle version du CLI d'Angular, et si nécessaire, la version de Node également. Pour cela :

1. Créer une branche sans MR.
1. Mettre à jour la version d'Angular dans `.gitlab-ci.yml` en modifiant la valeur de `ANGULAR_VERSION`.
1. Si nécessaire, mettre à jour le contenu de l'image de build construite par le fichier `gitlab/images-rebuild.yml`, la version de Node par exemple.
1. Commit et push de la branche
1. Reconstruire l'image par la CI : Lancer manuellement un pipeline sur la branche avec la variable `MISIS_IMAGE_REBUILD` avec la valeur `true`
1. Créer une MR et vérifier que tout se construit bien avec la nouvelle image avant de merge.
