#!/bin/sh
runtimeFiles=$(grep -lr "http://localhost:8080" /usr/share/nginx/html)
for f in ${runtimeFiles}
do
    sed -i -E "s@http://localhost:8080@${WEB_PROTOCOL}://${API_HOSTNAME}@g" "$f"
done
runtimeFiles=$(grep -lr "http://localhost:4200" /usr/share/nginx/html)
for f in ${runtimeFiles}
do
    sed -i -E "s@http://localhost:4200@${WEB_PROTOCOL}://${WEB_HOSTNAME}@g" "$f"
done
runtimeFiles=$(grep -lr "http://localhost:8088" /usr/share/nginx/html)
for f in ${runtimeFiles}
do
    sed -i -E "s@http://localhost:8088@${WEB_PROTOCOL}://${KEYCLOAK_HOSTNAME}@g" "$f"
done
runtimeFiles=$(grep -lr "backend-dev-service" /usr/share/nginx/html)
for f in ${runtimeFiles}
do
    sed -i -E "s@backend-dev-service@${KC_CLIENTID}@g" "$f"
done
if [ -n "${LOGS_TOKEN}" ]; then
    sed -i -E "s@user\s+nginx;@user root;@g" /etc/nginx/nginx.conf
fi