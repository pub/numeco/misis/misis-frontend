#!/bin/sh
if [ -n "${KEYCLOAK_HOSTNAME}" ]; then
    echo "
server {
  listen 80;
  server_name \${KEYCLOAK_HOSTNAME};  location / {
      proxy_pass \${KEYCLOAK_INTERNAL_URL};
      proxy_set_header    Host \$host;
      proxy_connect_timeout 30;
      proxy_send_timeout 30;
  }
}
" >> /etc/nginx/templates/default.conf.template
fi
