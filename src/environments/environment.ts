export const environment = {
    BASE_API: 'http://localhost:8080',
    WORDING: {
        LINK_TITLE: ' - MISIS - Ministères Aménagement du territoire Transition écologique',
        LOGO_TITLE: [
            "Ministères",
            "Aménagement",
            "du territoire",
            "Transition",
            "écologique"
        ].join("<br/>"),
        COOKIES_MANAGER_TITLE: 'À propos des cookies',
        COOKIES_MANAGER_CONTENT: 'Bienvenue ! Nous utilisons uniquement des cookies obligatoire pour améliorer votre expérience sur ce site. Pour en savoir plus, visitez la page <a href="/donnees-personnelles">Données personnelles et cookies</a>.',
    }
};
