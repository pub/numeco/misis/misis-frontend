import {ResourceDetailsDto} from "../../../rest/model/resource-details-dto";
import {PageDetailsDto} from "../../../rest/model/page-details-dto";
import {DatasetsDto} from "../../../rest/model/datasets-dto";

export const generateCSV = (objArray: ResourceDetailsDto[] | PageDetailsDto[] | DatasetsDto[]): string => {
  const array = [Object.keys(objArray[0])].concat(Object.values(objArray));
  return array.map(row => {
    return Object.values(row).map(value => `"${value}"`).join(',');
  }).join('\n');
}

export const generatePdf= (array: { [key: string]: any }[]): { [key: string]: string }[] => {
  return array.map(obj => {
    const result: { [key: string]: string } = {};
    for (const key in obj) {
      if (obj.hasOwnProperty(key)) {
        result[key] = String(obj[key]);
      }
    }
    return result;
  })
}

export const generatePdfHeader = (data: ResourceDetailsDto[] | PageDetailsDto[] | DatasetsDto[]):string[] => {
  return Object.keys(data[0]);
}
