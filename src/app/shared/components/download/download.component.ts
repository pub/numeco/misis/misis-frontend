import {Component, input} from '@angular/core';
import {ResourceDetailsDto} from "../../../rest/model/resource-details-dto";
import {PageDetailsDto} from "../../../rest/model/page-details-dto";
import {DatasetsDto} from "../../../rest/model/datasets-dto";
import {generateCSV, generatePdf, generatePdfHeader} from "./file-generator";
import moment from "moment";
import jsPDF from 'jspdf';
import {FormsModule} from "@angular/forms";

@Component({
  selector: 'misis-download',
  standalone: true,
  imports: [
    FormsModule
  ],
  template: `
    <ul class="fr-btns-group fr-col fr-col-offset-1">
      <li>
        <div class="fr-select-group" [class.fr-select-group--error]="notSelected">
          <label class="fr-label" for="select">
            Format du téléchargement des données
          </label>
          <select class="fr-select" id="select" name="select" [(ngModel)]="selectedOption">
            @for (option of options; track $index) {
              <option [value]="option.value" [innerText]="option.label" [attr.selected]="$index===0">
              </option>
            }
          </select>
          @if (notSelected) {
            <p id="select-error-desc-error" class="fr-error-text">
              Merci de sélectionner un format
            </p>
          }
        </div>
        <button class="fr-btn" (click)="download()">Télécharger</button>
      </li>
    </ul>
  `,
  styles: ``
})
export class DownloadComponent {
  fileSuffix = input.required<string>()
  data = input.required<Array<ResourceDetailsDto> | Array<PageDetailsDto> | Array<DatasetsDto> | undefined>();

  notSelected = false;
  options = [
    { value: 'csv', label: 'CSV' },
    { value: 'pdf', label: 'PDF' },
  ];
  selectedOption: string = 'csv';

  download() {
    if (this.selectedOption) {
      switch (this.selectedOption) {
        case 'csv':
          this.downloadCSV();
          break
        case 'pdf':
          this.downloadPDF();
          break
      }
    } else {
      this.notSelected=true;
    }
  }

  fileName = () => this.fileSuffix()+' - '+ moment().format("DD-MM-YYYYTHH-mm-ss")

  downloadCSV() {
    if (this.data()) {
      const csvData = generateCSV(this.data()!);
      const blob = new Blob([csvData], { type: 'text/csv' });
      const url = window.URL.createObjectURL(blob);
      const a = document.createElement('a');
      a.href = url;
      a.download = this.fileName()+'.csv';
      a.click();
      window.URL.revokeObjectURL(url);
    }
  }

  downloadPDF(): void {
    if (this.data()) {
      const doc = new jsPDF({
        orientation:"landscape",
        compress:true,
      });

      doc.table(5, 5, generatePdf(this.data()! as { [key: string]: any }[]), generatePdfHeader(this.data()!), {autoSize:true,fontSize:10});
      doc.save(this.fileName()+'.pdf');
    }
  }
}
