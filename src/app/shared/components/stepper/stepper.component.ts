import {Component, input} from '@angular/core';

@Component({
  selector: 'misis-stepper',
  standalone: true,
  imports: [],
  template: `
    <div class="fr-stepper">
      <h2 class="fr-stepper__title">
        {{ currentTitle() }}
        <span class="fr-stepper__state">
          Étape {{ currentStep() }} sur {{ stepTitles().length }}
        </span>
      </h2>
      <div class="fr-stepper__steps" [attr.data-fr-current-step]="currentStep()" [attr.data-fr-steps]="stepTitles().length"></div>
      @if (nextTitle()) {
        <p class="fr-stepper__details">
          <span class="fr-text--bold">
            Étape suivante :
          </span>
          {{ nextTitle() }}
        </p>
      }
    </div>
  `,
  styles: ``
})
export class StepperComponent {
  stepTitles = input.required<string[]>()
  currentStep = input.required<number>()

  currentTitle = () => {
    return this.stepTitles()[this.currentStep()-1];
  }

  nextTitle = () => {
    const currentStep = this.currentStep();
    if (currentStep >= this.stepTitles().length) {
      return null;
    }
    return this.stepTitles()[currentStep];
  }
}
