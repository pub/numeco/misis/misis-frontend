export class DataType {
  static readonly BYTE: Intl.NumberFormatOptions = {
    style: 'unit',
    unit: 'byte',
    unitDisplay: 'narrow',
    notation: "compact",
    maximumFractionDigits: 2,
  };

  static readonly NUMBER: Intl.NumberFormatOptions = {
  };

}
