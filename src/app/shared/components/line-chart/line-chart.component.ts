import {Component, inject, input, OnInit, ViewChild} from '@angular/core';
import {BaseChartDirective} from "ng2-charts";
import {DatePipe, NgClass} from "@angular/common";
import moment from "moment/moment";
import {ChartConfiguration, ChartDataset} from "chart.js";
import {StatisticsDto} from "../../../rest/model/statistics-dto";
import 'chartjs-adapter-moment';
import 'moment/locale/fr';
import {ChartsUnit} from "./charts-unit";
import {AbstractControl, FormBuilder, ReactiveFormsModule, ValidatorFn} from "@angular/forms";
import {DurationInputArg2} from "moment";

@Component({
  selector: 'misis-line-chart',
  standalone: true,
  imports: [
    BaseChartDirective,
    DatePipe,
    ReactiveFormsModule,
    NgClass
  ],
  template: `
    @if (enableDateSelector()) {
      <fieldset [formGroup]="dateForm" class="fr-fieldset" id="input-inline" aria-labelledby="input-inline-legend input-inline-messages">
        <legend class="fr-fieldset__legend--regular fr-fieldset__legend fr-text--bold" id="input-inline-legend">
          Résultats sur la période
        </legend>
        <div class="fr-fieldset__element fr-fieldset__element--inline"
             [ngClass]="{ 'fr-fieldset--error': !dateForm.valid }"
        >
          <input class="fr-input fr-input--error" id="text-input-start-date" step="1" formControlName="startDate"
                 [type]="unit().htmlValue"
                 [min]="data().labels![0] | date: unit().dateFormat"
                 [max]="data().labels![data().labels!.length-1] | date: unit().dateFormat"
          >
        </div>
        <div class="fr-fieldset__element fr-fieldset__element--inline"
             [ngClass]="{ 'fr-fieldset--error': !dateForm.valid }"
        >
          <input class="fr-input" id="text-input-end-date" formControlName="endDate"
                 [type]="unit().htmlValue"
                 [min]="data().labels![0] | date: unit().dateFormat"
                 [max]="data().labels![data().labels!.length-1] | date: unit().dateFormat"
          >
        </div>
        @if (dateForm.errors) {
          <div class="fr-messages-group" id="input-inline-messages" aria-live="assertive">
            @if (dateForm.errors['dateRangeInvalid']) {
              <div class="fr-message--error">
                Les dates ne doivent pas se chevaucher.
              </div>
            }
          </div>
        }
      </fieldset>
    }
    <div style="display: block;">
      <h3 class="fr-my-2w">{{ title() }}</h3>
      <p class="fr-text--sm">{{ subtitle() }}</p>
      <canvas
        baseChart
        [data]="lineChartData"
        [options]="lineChartOptions"
        [type]="'line'"
      ></canvas>
      <fieldset class="fr-fieldset" id="checkboxes-inline" aria-labelledby="checkboxes-inline-legend checkboxes-inline-messages">
        @if (!hideSwitchInfo()) {
          <legend class="fr-fieldset__legend--regular fr-fieldset__legend" id="checkboxes-inline-legend">
            Pour masquer ou afficher une courbe, cliquez sur la légende correspondante.
          </legend>
        }
        @for (dataset of lineChartData.datasets; track $index) {
          <div class="fr-fieldset__element fr-fieldset__element--inline">
            <div class="fr-checkbox-group">
              <input name="checkboxes-inline-{{$index}}" id="checkboxes-inline-{{$index}}" type="checkbox" [attr.aria-describedby]="'checkboxes-inline-'+$index+'-messages'"
                     (change)="hideDataset($index)"
              >
              <label class="fr-label" for="checkboxes-inline-{{$index}}">
                {{ dataset.label }}
              </label>
              <div class="fr-messages-group" id="checkboxes-inline-{{$index}}-messages" aria-live="assertive"></div>
            </div>
          </div>
        }
        <div class="fr-messages-group" id="checkboxes-inline-messages" aria-live="assertive"></div>
      </fieldset>
    </div>`,
  styles: ``
})
export class LineChartComponent implements OnInit {
  fb = inject(FormBuilder)

  data = input.required<StatisticsDto>();
  title = input.required<string>();
  subtitle = input.required<string>();
  enableDateSelector = input<boolean>();
  unit = input.required<ChartsUnit>();
  lineStyle = input.required<ChartDataset<'line', number[]>[]>();
  dataType = input.required<Intl.NumberFormatOptions>();
  hideSwitchInfo = input<boolean>();

  @ViewChild(BaseChartDirective) chart?: BaseChartDirective;

  lineChartData: ChartConfiguration['data'] = {
    datasets: [],
    labels: [],
  };

  lineChartOptions: ChartConfiguration['options'];

  ngOnInit(): void {
    this.addChartsDataWithStyle();
    this.dateForm.controls.startDate.setValue(moment(this.data().labels![0]).format(this.unit().dateFormat));
    this.dateForm.controls.endDate.setValue(moment(this.data().labels![this.data().labels!.length - 1]).format(this.unit().dateFormat));
  }

  private addChartsDataWithStyle() {
    this.lineChartData.datasets = this.lineStyle();
    this.lineChartOptions = {
      scales: {
        x: {
          type: 'timeseries',
          time: {
            unit: this.unit().chartsValue,
            displayFormats: {
              day: 'D MMM YYYY',
              month: 'MMM YYYY',
              year: 'YYYY'
            },
            tooltipFormat: 'll',
          },
        },
        y: {
          ticks: {
            format: this.dataType(),
          },
        },
      },
      plugins: {
        legend: {
          display: false,
          position: 'bottom',
          align: 'start',
          labels: {
            boxWidth: 12,
            usePointStyle: true,
            font: {
              family: 'Marianne',
              weight: 'bold',
            }
          }
        },
      },
    };
    const datasets = this.restrictsTheNumberOfLinesIfAllValuesAreTheSame(this.data()).datasets!
      .filter((value, index) => this.lineChartData.datasets.length > index)
      .map((value, index) => this.mergeObjects(this.lineChartData.datasets[index], value));
    this.lineChartData = {
      ...this.lineChartData,
      datasets: datasets,
      labels: this.data().labels,
    }
  }

  restrictsTheNumberOfLinesIfAllValuesAreTheSame = (stats: StatisticsDto): StatisticsDto => {
    if (!stats || !stats.datasets || stats.datasets && stats.datasets.length !== 3) {
      return stats;
    }
    let hasSameValues = false;
    for (let i = 0; i < stats.datasets.at(2)!.data!.length; i++) {
      const value = stats.datasets.at(2)!.data!.at(i);
      hasSameValues = value === stats.datasets.at(1)!.data!.at(i) && value === stats.datasets.at(0)!.data!.at(i);

      if (!hasSameValues) {
        return stats;
      }
    }
    stats.datasets = [stats.datasets[2]]
    return stats;
  }

  dateRangeValidator: ValidatorFn = (control: AbstractControl): { [key: string]: any } | null => {
    const startDate = control.get("startDate")
    const endDate = control.get("endDate")

    if (startDate?.value && endDate?.value && startDate?.value > endDate?.value) {
      if (startDate.dirty) {
        startDate.setValue(endDate.value)
      } else if (endDate.dirty) {
        endDate.setValue(startDate.value)
      }
      control.markAsPristine()
      return {'dateRangeInvalid': true}
    } else {
      control.markAsPristine()
    }

    this.updateChart(startDate?.value, endDate?.value)
    return null;
  };

  updateChart = (startDate: string | number, endDate: string | number) => {
    if (this.lineChartOptions?.scales?.['x']) {
      const startDateTime = moment(startDate || this.data().labels![0], this.unit().dateFormat).toDate().getTime()
      const endDateTime = moment(endDate || this.data().labels![this.data().labels!.length - 1], this.unit().dateFormat).add(1, this.unit().chartsValue as DurationInputArg2).add(-1, "milliseconds").toDate().getTime()

      this.lineChartOptions = {
        ...this.lineChartOptions,
        scales: {
          ...this.lineChartOptions.scales,
          x: {
            ...this.lineChartOptions.scales['x'],
            min: startDateTime,
            max: endDateTime,
          }
        }
      }
      this.chart?.update()
    }
  }

  dateForm = this.fb.group({
    startDate: [''],
    endDate: [''],
  }, {
    validators: this.dateRangeValidator,
  });

  mergeObjects = (...objects: Array<any>) => {
    let target: any = {};
    for (let obj of objects) {
      for (let prop in obj) {
        if (obj.hasOwnProperty(prop)) {
          target[prop] = obj[prop];
        }
      }
    }
    return target;
  };

  hideDataset = (index: number) => {
    this.chart?.hideDataset(index, !this.chart?.isDatasetHidden(index));
    this.chart?.update();
  }
}
