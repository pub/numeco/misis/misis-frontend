import {TimeUnit} from "chart.js/dist/core/core.adapters";

export class ChartsUnit {
  static readonly DAY = new ChartsUnit('day', 'date', 'yyyy-MM-DD');
  static readonly MONTH = new ChartsUnit('month', 'month', 'yyyy-MM');
  static readonly YEAR = new ChartsUnit('year', 'number', 'yyyy');

  chartsValue;
  htmlValue;
  dateFormat;

  private constructor(chartsValue:false | TimeUnit, htmlValue:string, dateFormat:string) {
    this.chartsValue = chartsValue;
    this.htmlValue = htmlValue;
    this.dateFormat = dateFormat;
  }
}
