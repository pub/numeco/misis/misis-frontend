import {ChartDataset} from "chart.js";

export class LineStyle {
  public static readonly dashedBlue : ChartDataset<'line', number[]> = {
    data: [],
    label: '',
    backgroundColor: getComputedStyle(document.body).getPropertyValue('--blue-cumulus-main-526'),
    borderColor: getComputedStyle(document.body).getPropertyValue('--blue-cumulus-main-526'),
    pointStyle: 'rect',
    borderDash: [6, 2],
    borderWidth: 2,
    pointBackgroundColor: getComputedStyle(document.body).getPropertyValue('--blue-cumulus-main-526'),
  }
  public static readonly dashedGreen : ChartDataset<'line', number[]> = {
    data: [],
    label: '',
    backgroundColor: getComputedStyle(document.body).getPropertyValue('--green-bourgeon-main-640'),
    borderColor: getComputedStyle(document.body).getPropertyValue('--green-bourgeon-main-640'),
    pointStyle: 'rect',
    borderDash: [6, 2],
    borderWidth: 2,
    pointBackgroundColor: getComputedStyle(document.body).getPropertyValue('--green-bourgeon-main-640'),
  }

  public static readonly dashedOrange : ChartDataset<'line', number[]> = {
    data: [],
    label: '',
    backgroundColor: getComputedStyle(document.body).getPropertyValue('--orange-terre-battue-950'),
    borderColor: getComputedStyle(document.body).getPropertyValue('--orange-terre-battue-950'),
    pointStyle: 'rect',
    borderDash: [5, 5],
    pointBackgroundColor: getComputedStyle(document.body).getPropertyValue('--orange-terre-battue-950'),
  }

  public static readonly pink : ChartDataset<'line', number[]> = {
    data: [],
    label: '',
    backgroundColor: getComputedStyle(document.body).getPropertyValue('--pink-tuile-main-556'),
    borderColor: getComputedStyle(document.body).getPropertyValue('--pink-tuile-main-556'),
    pointStyle: 'rect',
    pointBackgroundColor: getComputedStyle(document.body).getPropertyValue('--pink-tuile-main-556'),
  }
}
