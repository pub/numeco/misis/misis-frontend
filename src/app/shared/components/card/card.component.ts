import {Component, input} from '@angular/core';
import {NgOptimizedImage} from "@angular/common";
import {RouterLink} from "@angular/router";

let nextId = 0;

@Component({
    selector: 'misis-card',
    standalone: true,
    imports: [
        NgOptimizedImage,
        RouterLink
    ],
    template: `
        <div class="fr-card fr-enlarge-link">
            <div class="fr-card__body">
                <div class="fr-card__content">
                    <h3 class="fr-card__title">
                        <a [routerLink]="routerLink()">{{ titre() }}</a>
                    </h3>
                    <div class="fr-card__desc">
                        <p>{{ sousTitre() }}</p>
                        <ng-content></ng-content>
                    </div>
                    <div class="fr-card__end">
                        <p class="fr-card__detail">{{ detail() }}</p>
                    </div>
                </div>
            </div>
            <div class="fr-card__header">
                @if (imagePath()) {
                    <div class="fr-card__img">
                        <img class="fr-responsive-img" [ngSrc]="imagePath()!" alt=""/>
                        <!-- L’alternative de l’image (attribut alt) doit toujours être présente, sa valeur peut-être vide (image n’apportant pas de sens supplémentaire au contexte) ou non (porteuse de texte ou apportant du sens) selon votre contexte -->
                    </div>
                }

            </div>
        </div>
    `,
    styles: ``
})
export class CardComponent {

    titre = input.required<string>();
    routerLink = input.required<(string | number)[]>();
    sousTitre = input<string>();
    detail = input<string>();
    imagePath = input<string>();

    id = nextId++;

}
