import { booleanAttribute, Component, input, Optional, output } from '@angular/core';
import { ControlValueAccessor, FormControl, NgControl, ReactiveFormsModule } from "@angular/forms";
import { takeUntilDestroyed } from "@angular/core/rxjs-interop";
import { InputAddonComponent } from "../input-addon/input-addon.component";

@Component({
  selector: 'misis-input-text',
  standalone: true,
  imports: [
    ReactiveFormsModule,
    InputAddonComponent
  ],
  template: `
      <div class="fr-input-group"
           [class.fr-input-group--error]="ngControl.control?.errors && (ngControl.control?.touched || ngControl.control?.dirty)"
           [class.fr-input-group--valid]="ngControl.control?.valid && ngControl.control?.touched"
      >
        @if (label()) {
          <label [id]="'label-'+ngControl.name" class="fr-label" [class.required]="required()" for="form-ctrl">
            {{ label()}}
            @if (tooltip()) {
              <button class="fr-btn--tooltip fr-btn" type="button" id="button-2995" aria-describedby="tooltip-label">
                  Information complémentaire au champ de saisie
              </button>
              <span class="fr-tooltip fr-placement" id="tooltip-label" role="tooltip" aria-hidden="true">{{ tooltip() }}</span>
            }
          </label>
          @if (hint()) {
            <span class="fr-hint-text">{{hint()}}</span>
          }
        }

        <misis-input-addon [addon]="addon()">
            <input class="fr-input" type="text" id="form-ctrl" placeholder="{{placeholder() ? placeholder() : ''}}"
                   (blur)="onTouched()"
                   [class.fr-input--error]="ngControl.control?.errors && (ngControl.control?.touched || ngControl.control?.dirty)"
                   [class.fr-input--valid]="ngControl.control?.valid && ngControl.control?.touched"
                   [formControl]="inputCtrl" [required]="required()"
                   (keydown.enter)="emitKeypressEnter($event)"
                   [attr.aria-labelledby]="'label-'+ngControl.name"
                   [attr.aria-required]="required()"
            >

          </misis-input-addon>
        <ng-content></ng-content>
      </div>
    `,
  styles: `
      .fr-btn--tooltip  {
        position: absolute;
        top: -.3rem;
      }
    `
})
export class InputTextComponent implements ControlValueAccessor {

  required = input(false, { transform: booleanAttribute });
  label = input<string>();
  hint = input<string>();
  addon = input<string>();
  placeholder = input<string>();
  tooltip = input<string>();
  keydownEnter = output<Event>()
  inputCtrl = new FormControl<string>('', { nonNullable: true });

  constructor(@Optional() public ngControl: NgControl) {
    this.ngControl.valueAccessor = this;

    this.inputCtrl.valueChanges
      .pipe(takeUntilDestroyed())
      .subscribe(value => {
        this.onChange(value);
        this.onTouched();
      });
  }

  emitKeypressEnter = ($event: Event) => {
    this.keydownEnter.emit($event);
  }

  onChange: (value: string) => void = () => {
  };
  onTouched: () => void = () => {
  };

  registerOnChange(fn: (value: string) => void): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: () => void): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    if (isDisabled) {
      this.inputCtrl.disable();
    } else {
      this.inputCtrl.enable();
    }
  }

  writeValue(value: string): void {
    this.inputCtrl.setValue(value, { emitEvent: false });
  }

}
