import {Injectable} from '@angular/core';
import {map, Observable} from "rxjs";
import {Data, RouteConfigLoadEnd, Router} from "@angular/router";
import {filter} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class RouterDataService {
  data$:Observable<Data | undefined>;

  constructor(private router: Router) {
    this.data$ = router.events.pipe(
      filter(event => event instanceof RouteConfigLoadEnd),
      map(event => event as RouteConfigLoadEnd),
      map(event => event.route.data),
    );
  }

}
