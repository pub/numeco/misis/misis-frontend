import {Component, ElementRef, inject, ViewChild} from '@angular/core';
import {RouterDataService} from "./router-data.service";
import {AsyncPipe} from "@angular/common";
import {map, Observable, of} from "rxjs";
import {filter} from "rxjs/operators";
import {SkipLinks} from "./skip-links";
import {ActivatedRoute, NavigationEnd, Router, RouterLink} from "@angular/router";
import {fragment} from "@angular-devkit/core";

@Component({
  selector: 'misis-skip-links',
  standalone: true,
  imports: [
    AsyncPipe,
    RouterLink
  ],
  template: `
    @if (skipLinks | async; as skipLinks) {
      <div class="fr-skiplinks" #focus>
        <nav class="fr-container" role="navigation" aria-label="Accès rapide">
          <ul class="fr-skiplinks__list">
            @for (skiplink of skipLinks; track skiplink) {
              <li>
                <a class="fr-link" [routerLink]="[]" fragment="{{ skiplink.anchor }}">{{ skiplink.title }}</a>
              </li>
            }
          </ul>
        </nav>
      </div>
    }
  `,
  styles: ``
})
export class SkipLinksComponent {
  routerDataService = inject(RouterDataService)
  router = inject(Router)
  activatedRoute=inject(ActivatedRoute);

  skipLinks: Observable<SkipLinks[]> = this.routerDataService.data$.pipe(
    filter(value => value !== undefined),
    map(value => value!['skiplinks'])
  )

  constructor() {
    this.activatedRoute.fragment.subscribe(fragment => {
      if (fragment) {
        const element = document.getElementById(fragment);
        if (element) {
          element.scrollIntoView({ behavior: 'smooth' });
        }
      }
    });
  }
}
