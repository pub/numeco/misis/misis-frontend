import {SkipLinks} from "./skip-links";

export const generateSkipLinks = (innerSkipLinks: SkipLinks[]=[]): SkipLinks[] => [
    {
        title:"Contenu",
        anchor:"main-content"
    },
    {
        title: "En-tête",
        anchor: "header"
    },
    ...innerSkipLinks,
    {
        title: "Pied de page",
        anchor: "footer"
    }
]
