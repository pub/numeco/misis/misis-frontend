import {Component, input} from '@angular/core';
import {ReactiveFormsModule} from "@angular/forms";

@Component({
  selector: 'misis-input-addon',
  standalone: true,
  imports: [
    ReactiveFormsModule
  ],
  template: `
    <div [class.fr-input-wrap--addon]="addon()" class="{{ addon() == 'https://' ? '' : 'fr-input-wrap--addon-domain' }}">
      @if (addon()) {
        <span class="fr-btn fr-btn--tertiary">{{ addon() }}</span>
      }
      <ng-content></ng-content>
    </div>
  `,
  styles: `
    .fr-input-wrap--addon span {
      white-space: nowrap;
    }

    @media (max-width: 768px) {
      .fr-input-wrap--addon-domain {
        display: flex;
        flex-direction: column;
        span {
          white-space: wrap;
          display: block;
          width: 100%;
        }
      }
    }
  `
})
export class InputAddonComponent {
  addon = input<string>()
}
