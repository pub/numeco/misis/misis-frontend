import {ChangeDetectionStrategy, Component, input} from '@angular/core';

@Component({
    selector: 'misis-input-error',
    standalone: true,
    imports: [],
    template: `
        <p id="text-input-error-desc-error" class="fr-error-text" role="alert">
            {{ message() }}
        </p>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InputErrorComponent {
    message = input<string>('Ce champ est obligatoire.');
}
