import { Component, signal } from '@angular/core';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'misis-cookies-manager',
  standalone: true,
  imports: [],
  template: `
    @if (visible()) {
      <div class="fr-consent-banner">
        <h2 class="fr-h6" [innerHTML]="wording.COOKIES_MANAGER_TITLE">
        </h2>
        <div class="fr-consent-banner__content">
          <p class="fr-text--sm" [innerHTML]="wording.COOKIES_MANAGER_CONTENT">
          </p>
        </div>
        <ul class="fr-consent-banner__buttons fr-btns-group fr-btns-group--right fr-btns-group--inline-reverse fr-btns-group--inline-sm">
          <li>
              <button class="fr-btn" title="Autoriser tous les cookies" (click)="hideBanner()">
                  Tout accepter
              </button>
          </li>
          <li>
              <button class="fr-btn" title="Refuser tous les cookies" (click)="hideBanner()">
                  Tout refuser
              </button>
          </li>
          <li>
              <button class="fr-btn fr-btn--secondary" data-fr-opened="false" aria-controls="fr-consent-modal" title="Personnaliser les cookies">
                  Personnaliser
              </button>
          </li>
        </ul>
      </div>
    }
    <dialog id="fr-consent-modal" class="fr-modal" role="dialog" aria-labelledby="fr-consent-modal-title">
    <div class="fr-container fr-container--fluid fr-container-md">
        <div class="fr-grid-row fr-grid-row--center">
            <div class="fr-col-12 fr-col-md-10 fr-col-lg-8">
                <div class="fr-modal__body">
                    <div class="fr-modal__header">
                        <button class="fr-btn--close fr-btn" aria-controls="fr-consent-modal" title="Fermer">
                            Fermer
                        </button>
                    </div>
                    <div class="fr-modal__content">
                        <h1 id="fr-consent-modal-title" class="fr-modal__title">
                            Panneau de gestion des cookies
                        </h1>
                        <div class="fr-consent-manager">
                            <div class="fr-consent-service">
                                <fieldset aria-labelledby="finality-0-legend finality-0-desc" role="group"
                                    class="fr-fieldset fr-fieldset--inline">
                                    <legend id="finality-0-legend" class="fr-consent-service__title">Cookies
                                        obligatoires</legend>
                                    <div class="fr-consent-service__radios">
                                        <div class="fr-radio-group">
                                            <input type="radio" id="consent-finality-0-accept"
                                                name="consent-finality-0" checked>
                                            <label class="fr-label" for="consent-finality-0-accept">
                                                Accepter
                                            </label>
                                        </div>
                                        <div class="fr-radio-group">
                                            <input disabled type="radio" id="consent-finality-0-refuse"
                                                name="consent-finality-0">
                                            <label class="fr-label" for="consent-finality-0-refuse">
                                                Refuser
                                            </label>
                                        </div>
                                    </div>
                                    <p id="finality-0-desc" class="fr-consent-service__desc">
                                        Ce site utilise des cookies nécessaires à son bon fonctionnement qui ne peuvent
                                        pas être désactivés.
                                    </p>
                                </fieldset>
                            </div>
                            <ul
                                class="fr-consent-manager__buttons fr-btns-group fr-btns-group--right fr-btns-group--inline-sm">
                                <li>
                                  <button class="fr-btn" aria-controls="fr-consent-modal"> 
                                    Confirmer mes choix
                                  </button>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</dialog>
  `,
  styles: ``
})
export class CookiesManagerComponent {
  wording = environment.WORDING;
  visible = signal<boolean>(true);
  constructor() {
    const isHidden = sessionStorage.getItem('cookie_manager_hidden');
    if (isHidden) {
      this.visible.set(false);
    }
  }

  hideBanner() {
    sessionStorage.setItem('cookie_manager_hidden', 'true');
    this.visible.set(false);
  }
}
