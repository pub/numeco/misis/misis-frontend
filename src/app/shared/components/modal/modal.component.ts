import {Component, Input, input} from '@angular/core';

@Component({
  selector: 'misis-modal',
  standalone: true,
  imports: [],
  template: `
    @if (modalOpeningLabel()) {
      <button class="fr-btn" data-fr-opened="true" [attr.aria-controls]="id()">
        {{ modalOpeningLabel() }}
      </button>
    }
    <dialog [attr.aria-labelledby]="id()+'-title'" [id]="id()" class="fr-modal" role="dialog" [class.fr-modal--opened]="isOpenModal()" (click)="requestFromBackdropToClose($event.target)">
      <div class="fr-container fr-container--fluid fr-container-md">
        <div class="fr-grid-row fr-grid-row--center">
          <div class="fr-col-12 fr-col-md-8 fr-col-lg-6">
            <div class="fr-modal__body">
              <div class="fr-modal__header">
                @if (modalClosingLabelInHeader()) {
                  @if (modalOpeningLabel()) {
                    <button class="fr-btn--close fr-btn" [attr.aria-controls]="id()" [attr.data-fr-opened]="true" (click)="secondaryAction()">
                      {{ modalClosingLabelInHeader() }}
                    </button>
                  } @else {
                    <button class="fr-btn--close fr-btn" (click)="secondaryAction()">
                      {{ modalClosingLabelInHeader() }}
                    </button>
                  }
                }
              </div>
              <div class="fr-modal__content">
                <h1 [id]="id()+'-title'" class="fr-modal__title">
                <span class="fr-icon-arrow-right-line fr-icon--lg">
                </span>
                  {{ modalTitle() }}
                </h1>

                <ng-content></ng-content>
              </div>
              @if (primaryLabel() && secondaryLabel()) {
                <div class="fr-modal__footer">
                  <div class="fr-btns-group fr-btns-group--right fr-btns-group--inline-reverse fr-btns-group--inline-lg fr-btns-group--icon-left">
                    @if (primaryLabel()) {
                      <button class="fr-btn fr-icon-checkbox-circle-line fr-btn--icon-left" (click)="primaryAction()">
                        {{ primaryLabel() }}
                      </button>
                    }
                    @if (secondaryLabel()) {
                      <button class="fr-btn fr-icon-checkbox-circle-line fr-btn--icon-left fr-btn--secondary" (click)="secondaryAction()">
                        {{ secondaryLabel() }}
                      </button>
                    }
                  </div>
                </div>
              }
            </div>
          </div>
        </div>
      </div>
    </dialog>
  `,
  styles: ``
})
export class ModalComponent {
  id=input.required<string>()
  modalOpeningLabel=input<string>()
  isOpenModal = input<boolean>(false)
  modalClosingLabelInHeader = input<string>()
  modalTitle= input.required<string>()
  primaryLabel= input<string>()
  @Input() primaryAction: () => void;
  secondaryLabel= input<string>()
  @Input() secondaryAction: () => void;

  requestFromBackdropToClose(target: EventTarget | null) {
    if (target instanceof HTMLElement) {
      if (target.tagName.toLowerCase() === 'dialog') {
        this.secondaryAction();
      }
    }
  }
}
