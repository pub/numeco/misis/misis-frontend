import {Component, input, OnInit} from '@angular/core';

@Component({
  selector: 'misis-loading-alert',
  standalone: true,
  imports: [],
  template: `
    <div [class]="classes" role="alert">
        <p>{{ text() }}</p>
    </div>
  `,
  styles: ``
})
export class LoadingAlertComponent implements OnInit {
  constClasses = ['fr-alert', 'fr-mt-3w'];
  classes = <string[]>[];
  text = input<string>();
  class = input<string>();

  ngOnInit(): void {
    this.setAlertStyle();
  }

  private setAlertStyle()  {
    let alertStyle = this.class()!;
    this.classes = this.constClasses;
    this.classes.push(alertStyle);
  }

}
