export class AlertType {
  static readonly LOADING = {
    text: 'Chargement des données...',
    class: 'fr-alert--info',
  };
  static readonly FIRST_ANALYZIS = {
    text: 'La première analyse est en cours.',
    class: 'fr-alert--info',
  };
  static readonly NO_DATA = {
    text: 'Il n\'y a pas de données à afficher.',
    class: 'fr-alert--warning',
  };
}
