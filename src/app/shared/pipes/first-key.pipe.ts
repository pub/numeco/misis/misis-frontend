import {Pipe, PipeTransform} from '@angular/core';
import {GroupDetailsDto} from "../../rest/model/group-details-dto";

@Pipe(
  {
    standalone: true,
    name: 'firstKey'
  }
)
export class FirstKeyPipe implements PipeTransform {
  transform(value: { [p: string]: GroupDetailsDto } | undefined): string {
    if (!value) return "";

    let keys = Object.keys(value);
    return keys[0];
  }
}
