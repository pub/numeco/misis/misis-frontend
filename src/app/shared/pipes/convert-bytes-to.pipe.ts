import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'convertBytesTo',
    standalone: true
})
export class ConvertBytesToPipe implements PipeTransform {

    transform(bytes: number, unit: 'KB' | 'MB' | 'GB'): unknown {
        if (isNaN(bytes) || bytes === 0) {
            return '0 Bytes';
        }

        const units = ['Bytes', 'KB', 'MB', 'GB'];
        const index = units.indexOf(unit.toUpperCase());

        if (index === -1) {
            throw new Error(`Invalid unit: ${unit}. Please provide one of the following units: 'BYTES', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'`);
        }

        const convertedBytes = bytes / Math.pow(1000, index);
        return `${convertedBytes.toFixed(2)} ${units[index]}`;
    }

}
