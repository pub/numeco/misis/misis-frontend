import {Pipe, PipeTransform} from '@angular/core';
import {GroupPages} from "../../views/protected/suivis/formulaire-creation-suivi/model/group-pages.model";


@Pipe({
    name: 'pagesCountFromGroupesDePages',
    standalone: true
})
export class PagesCountFromGroupesDePagesPipe implements PipeTransform {

    transform(groupesDePages: GroupPages[]): unknown {
        return groupesDePages.reduce((acc, groupe) => acc + groupe.pages.length, 0);
    }

}
