import {Pipe, PipeTransform} from '@angular/core';
import moment from "moment";
import 'moment/locale/fr';

@Pipe({
  standalone: true,
  name: 'timeAgoFromNow'
})
export class TimeAgoPipeFromNow implements PipeTransform {
  transform(value: Date): string {
    return moment(value).fromNow(true);
  }
}
