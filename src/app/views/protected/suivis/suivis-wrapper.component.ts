import {ChangeDetectionStrategy, Component, inject, OnInit, signal} from "@angular/core";
import {RouterLink} from "@angular/router";
import {AsyncPipe} from "@angular/common";
import {CarteDeSuiviComponent} from "./tableau-de-suivi/carte-de-suivi.component";
import {WebsiteResourceService} from "../../../rest/api/website-resource.service";
import {FormsModule} from "@angular/forms";
import {TableauDeSuiviComponent} from "./tableau-de-suivi/tableau-de-suivi.component";
import {BreakpointObserver} from '@angular/cdk/layout';

@Component({
    selector: 'misis-tableaux-de-suivi',
    standalone: true,
    imports: [
        RouterLink,
        AsyncPipe,
        CarteDeSuiviComponent,
        FormsModule,
        TableauDeSuiviComponent
    ],
    templateUrl: './suivis-wrapper.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export default class SuivisWrapperComponent implements OnInit {

    /** Dependencies */
    protected readonly websiteResourceService = inject(WebsiteResourceService);

    /** Suivis de site */
    protected readonly suivisDeSite$ = this.websiteResourceService.suivisDeSiteGet();

    /** Sélecteur d'affichage */
    selecteurAffichage = signal<'table' | 'cards'>('table');

    protected readonly Object = Object;

    readonly breakpoint$ = this.breakpointObserver
        .observe(['(max-width: 768px)']);

    constructor(private breakpointObserver: BreakpointObserver) {
    }

    ngOnInit(): void {
        this.breakpoint$.subscribe(() =>
            this.breakpointChanged()
        )
    }

    private breakpointChanged() {
        if(this.breakpointObserver.isMatched('(max-width: 768px)')) {
            this.selecteurAffichage.set('cards');
        } else {
            this.selecteurAffichage.set('table');
        }
    }

}
