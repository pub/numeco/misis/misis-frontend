import {ChangeDetectionStrategy, Component, input} from "@angular/core";
import {DatePipe} from "@angular/common";
import {FirstKeyPipe} from "../../../../shared/pipes/first-key.pipe";
import {RouterLink} from "@angular/router";
import {WebsiteSummaryDto} from "../../../../rest/model/website-summary-dto";
import {TimeAgoPipeFromNow} from "../../../../shared/pipes/time-ago-from-now.pipe";
import {Alert} from "../../../../rest/model/alert";

@Component({
    selector: 'misis-tableau-de-suivi',
    standalone: true,
  imports: [
    DatePipe,
    FirstKeyPipe,
    RouterLink,
    TimeAgoPipeFromNow
  ],
    template: `
        <div class="fr-table fr-col-12" data-fr-js-table="true">
            <div class="fr-table__wrapper">
                <div class="fr-table__container">
                    <div class="fr-table__content">
                        <table data-fr-js-table-element="true" class="fr-cell--multiline">
                            <caption hidden="" data-fr-js-table-caption="true">
                                Tableau des suivis
                            </caption>
                            <thead>
                            <tr>
                                @for (header of headers; track header) {
                                    <th scope="col">{{ header }}</th>
                                }
                            </tr>
                            </thead>
                            <tbody>
                                @for (suivi of suiviDeSite(); track suivi.id) {
                                    <tr>
                                        <td>
                                            @if (suivi.lastAnalysis) {
                                                {{ suivi.lastAnalysis | date : 'dd/MM/yyyy' }}
                                            } @else if(!suivi.alert) {
                                                <span class="fr-badge fr-badge--new fr-badge--sm">
                                                    Nouveau
                                                    <button class="fr-btn--tooltip fr-btn" type="button" id="button-nextAnalysis" [attr.aria-describedby]="'tooltip-nextAnalysis-'+suivi.id">
                                                        Information contextuelle
                                                    </button>
                                                    <span class="fr-tooltip fr-placement" [id]="'tooltip-nextAnalysis-'+suivi.id" role="tooltip" aria-hidden="true">
                                                        Prochaine analyse dans {{ suivi.nextAnalysis! | timeAgoFromNow }}
                                                    </span>
                                                </span>
                                            } @else {
                                                -
                                            }
                                        </td>
                                        <td>{{ suivi.name }}</td>
                                        <td class="fr-cell--right">{{ Object.keys(suivi.groups || {}).length }}</td>
                                        <td class="fr-cell--right">
                                            {{ suivi.pageCount }}
                                        </td>
                                      <td>
                                        @switch (suivi.alert) {
                                          @case (Alert.WARNING) {
                                            Certaines pages n’ont pas pu être scannées. Veuillez vérifier les erreurs dans le suivi de votre site.
                                          }
                                          @case (Alert.ERROR) {
                                            Toutes les pages n’ont pas pu être scannées. Veuillez vérifier les erreurs dans le suivi de votre site.
                                          }
                                        }
                                      </td>
                                        <td>
                                            <button class="fr-btn fr-btn--secondary fr-btn--icon-right fr-icon-arrow-right-line"
                                                    [routerLink]="['detail', suivi.id!, 'groupe',  suivi.groups | firstKey, 'liste-des-pages']"
                                                    title="Accéder aux détails">
                                                Accéder aux détails
                                            </button>
                                        </td>
                                    </tr>
                                }
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TableauDeSuiviComponent {

    /** Inputs */
    suiviDeSite = input.required<WebsiteSummaryDto[]>()

    protected readonly headers: string[] = ['Date de dernière analyse', 'Nom du suivi', 'Groupes', 'Pages', 'Statut', 'Actions'];
    protected readonly Object = Object;
  protected readonly Alert = Alert;
}
