import {ChangeDetectionStrategy, Component, computed, input, output} from '@angular/core';
import {CardComponent} from "../../../../shared/components/card/card.component";
import {DatePipe} from "@angular/common";
import {WebsiteSummaryDto} from "../../../../rest/model/website-summary-dto";
import {FirstKeyPipe} from "../../../../shared/pipes/first-key.pipe";

@Component({
    selector: 'misis-carte-de-suivi',
    standalone: true,
    imports: [
        CardComponent,
        DatePipe,
        FirstKeyPipe
    ],
    template: `
        <misis-card [titre]="tableauDeSuivi().name!"
                    [routerLink]="['detail', tableauDeSuivi().id!, 'groupe', tableauDeSuivi().groups | firstKey, 'liste-des-pages']"
                    [sousTitre]="tableauDeSuivi().url"
                    detail="Accéder au détail de l'analyse">

            @if (tableauDeSuivi().lastAnalysis) {
                <div class="fr-table fr-col-12" data-fr-js-table="true">
                  <div class="fr-table__wrapper">
                    <div class="fr-table__container">
                      <div class="fr-table__content">
                          <table>
                              <caption [hidden]="true">Tableau contenant un résumé de l'analyse</caption>
                              <thead>
                              <tr>
                                  <th scope="col">Dernière analyse</th>
                                  <th scope="col" class="fr-cell--right">Groupes</th>
                                  <th scope="col" class="fr-cell--right">Pages</th>
                              </tr>
                              </thead>
                              <tbody>
                              <tr>
                                  <td>{{ tableauDeSuivi().lastAnalysis | date: 'dd/MM/yyyy' }}</td>
                                  <td class="fr-cell--right">{{ nGroups() }}</td>
                                  <td class="fr-cell--right">{{ tableauDeSuivi().pageCount }}</td>
                              </tr>
                              </tbody>
                          </table>
                      </div>
                    </div>
                  </div>
                </div>
            } @else {
                <p>Il n'y a pas encore d'analyse pour ce suivi.</p>
            }
        </misis-card>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CarteDeSuiviComponent {
    /** Inputs */
    readonly tableauDeSuivi = input.required<WebsiteSummaryDto>();
    /** Outputs */
    readonly selected = output<Event>();

    readonly nGroups = computed(() => Object.keys(this.tableauDeSuivi().groups || {}).length);
}
