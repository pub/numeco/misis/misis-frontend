import {AbstractControl, FormArray, FormControl, FormGroup, Validators} from "@angular/forms";
import {Constants} from "../../../../constants";
import {takeUntilDestroyed} from "@angular/core/rxjs-interop";

export const creationStepTitles: string[] = [
  'Nom du site suivi',
  'Création de groupe et méthode d’analyse',
];

export const splitUrlControl = (control: AbstractControl): { [key: string]: any } | null => {
  const originalUrl = control.value;
  if (originalUrl) {
    const capture = originalUrl.match(/^(?:https?:\/\/)?([^\/]+)(\/.*)?/i)
    if (capture?.[2] && capture[1] !== control.value) {
      control.setValue(capture[2]);
    }
  }
  return null;
};

export const pageCreationForm = () => new FormGroup({
  id: new FormControl<number | null>(null),
  url: new FormControl('', [
    Validators.required,
    splitUrlControl,
    Validators.pattern(Constants.PATH_PATTERN),
  ]),
});

export const groupCreationForm = () => new FormGroup({
  id: new FormControl<number | null>(null),
  name: new FormControl('', Validators.pattern(Constants.NOT_BLANK)),
  periodiciteDuSuivi: new FormControl('', Validators.required),
  methodeDeCreationDeGroupe: new FormControl('', Validators.required),
  pages: new FormArray([pageCreationForm()]),
});

export const step1 = new FormGroup({
  name: new FormControl('', [Validators.required, Validators.pattern(Constants.NOT_BLANK)]),
});

export const step2 = () => new FormGroup({
  url: new FormControl('', [Validators.required, Validators.pattern(Constants.DOMAIN_PATTERN)]),
  groupOfPages: new FormArray<GroupCreationForm>([groupCreationForm()]),
});

export const creationForm = () => new FormGroup({
  step1: step1,
  step2: step2()
})

export const buildPagesFormGroup = () => new FormGroup({
  id: new FormControl<number | null>(null),
  url: new FormControl('', [
    Validators.required,
    (control: AbstractControl): { [key: string]: any } | null => {
      const originalUrl = control.value;
      const capture = originalUrl.match(/^(?:https?:\/\/)?([^\/]+)(\/.*)?/i)
      if (capture?.[2] && capture[1] !== control.value) {
        control.setValue(capture[2]);
      }
      return null;
    },
    Validators.pattern(Constants.PATH_PATTERN),
  ]),
});

export const buildGroupePagesFormGroupWith = (path: string) => new FormGroup({
  id: new FormControl<number | null>(null),
  name: new FormControl('Groupe de pages n°1', Validators.pattern(Constants.NOT_BLANK)),
  periodiciteDuSuivi: new FormControl('QUOTIDIEN', Validators.required),
  methodeDeCreationDeGroupe: new FormControl('MANUELLE', Validators.required),
  pages: new FormArray([buildPageFormGroupWith(path)]),
});

export const buildPageFormGroupWith = (path: string) => new FormGroup({
  id: new FormControl<number | null>(null),
  url: new FormControl<string | null>(path, [Validators.required, Validators.pattern(Constants.PATH_PATTERN)]),
});

export const managePagesOnUrlChanges = (step2: CreationStep2Form): void => {
  if (step2) {
    step2.controls.groupOfPages.disable();

    step2.controls.url.valueChanges
      .pipe(takeUntilDestroyed())
      .subscribe(value => {
        if (value) {
          const capture = value.match(/^(?:https?:\/\/)?([^\/]+)(\/.*)?/i)
          if (capture && capture?.[1] !== value) {
            setValue(step2.controls.url, capture[1])
          }

          const group: FormArray<GroupCreationForm> = step2.controls.groupOfPages;
          if (step2.controls.url.valid) {
            group.enable();
          } else {
            group.disable();
          }

          if (capture?.[2]) {
            if (group.length === 1 && group.at(0).controls.name.value === '') {
              const firstGroupControls = group.at(0).controls;
              setValue(firstGroupControls.name, 'Groupe de pages n°1')
              setValue(firstGroupControls.methodeDeCreationDeGroupe, 'MANUELLE')
              setValue(firstGroupControls.periodiciteDuSuivi, 'QUOTIDIEN')
              setValue(firstGroupControls.pages.at(0).controls.url, capture?.[2])
            } else {
              group.at(0).controls.pages.push(buildPageFormGroupWith(capture?.[2]))
            }
          }
        }
      });
  }
}

export const setValue = (formControl: FormControl, value: string): void => {
  if (formControl.value === value) {
    return;
  }
  formControl.setValue(value);
  formControl.markAsTouched();
}

export type GroupCreationForm = ReturnType<typeof groupCreationForm>;
export type CreationStep1Form = typeof step1;
export type CreationStep2Form = ReturnType<typeof step2>;
