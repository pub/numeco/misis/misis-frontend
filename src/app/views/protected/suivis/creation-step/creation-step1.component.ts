import {Component, input, output} from '@angular/core';
import {CreationStep1Form} from "./creation-step-configuration";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {InputErrorComponent} from "../../../../shared/components/input-error/input-error.component";
import {InputTextComponent} from "../../../../shared/components/input-text/input-text.component";

@Component({
  selector: 'div[misis-creation-step1]',
  standalone: true,
  imports: [
    FormsModule,
    InputErrorComponent,
    InputTextComponent,
    ReactiveFormsModule
  ],
  template: `
    <p class="fr-col-offset-md-2 fr-col-md-8 fr-text--lead">
      Pour paramétrer le suivi de site, il est nécessaire de donner des informations à MISIS pour procéder à l'analyse du site web. Indiquer en premier lieu, un nom pour votre suivi de site.
    </p>
    <form (ngSubmit)="processToNextStep()" [formGroup]="form()" class="fr-col-offset-md-3 fr-col-md-6">
      <fieldset class="fr-fieldset">
        <div class="fr-fieldset__element">
          <span class="fr-hint-text">Sauf mention contraire, tous les champs sont obligatoires.</span>
        </div>
        <div class="fr-fieldset__element">
          <misis-input-text label="Nom du suivi"
                            hint="Nom d'affichage du suivi de site"
                            formControlName="name"
                            [required]="true">
            @if (form().controls.name.touched) {
              @if (form().controls.name.errors?.['required']) {
                <misis-input-error/>
              }
              @if (form().controls.name.errors?.['pattern']) {
                <misis-input-error message="Ce champ ne doit pas être vide."/>
              }
            }
          </misis-input-text>
        </div>
        <div class="fr-fieldset__element">
          <ul class="fr-btns-group fr-btns-group--inline fr-btns-group--right">
            <li>
              <button type="submit" class="fr-btn">
                Suivant
              </button>
            </li>
          </ul>
        </div>
      </fieldset>
    </form>
  `,
  styles: ``
})
export class CreationStep1Component {
  form = input.required<CreationStep1Form>();
  processToStep = output<number>();

  processToNextStep = () => {
    this.form().markAllAsTouched();

    if (this.form().valid) {
      this.processToStep.emit(2)
    }
  }
}
