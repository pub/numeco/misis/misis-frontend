import { Component, input, output, signal } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { InputErrorComponent } from "../../../../shared/components/input-error/input-error.component";
import { InputTextComponent } from "../../../../shared/components/input-text/input-text.component";
import { buildPagesFormGroup, CreationStep2Form, groupCreationForm } from "./creation-step-configuration";

@Component({
  selector: 'div[misis-creation-step2]',
  standalone: true,
  imports: [
    FormsModule,
    ReactiveFormsModule,
    InputTextComponent,
    InputErrorComponent,
  ],
  template: `
    <p [class.fr-col-offset-md-2]="centerInPage()" class="fr-col fr-col-md-8 fr-text--lead">
      La création d'un groupe de pages permet à MISIS de faire l'analyse d'un ensemble de pages qui ont la même périodicité de suivi (quotidien, hebdomadaire, mensuel).
      Pour la création du groupe de pages , renseigner un nom, une périodicité et une méthode d'analyse.
    </p>
    <form (ngSubmit)="processToNextStep($event)" [formGroup]="form()">
      <div class="fr-grid-row">
        <fieldset [class.fr-col-offset-md-3]="centerInPage()" class="fr-fieldset fr-col-md-6">
          <legend class="fr-fieldset__legend">
            <h6>
              Le site web de votre suivi
            </h6>
          </legend>
          <div class="fr-fieldset__element">
          <span class="fr-hint-text">
            Sauf mention contraire, tous les champs sont obligatoires.
          </span>
          </div>
          <div class="fr-fieldset__element">
            <misis-input-text
              formControlName="url"
              label="Nom de domaine"
              hint="La racine de votre site, par exemple https://votre-site.fr"
              addon="https://"
              tooltip="Seule la racine du site sera prise en compte ; le cas échéant, un groupe sera automatiquement créé avec la page saisie"
              [required]="true"
            >
              @if (form().controls.url.errors?.['required'] && form().controls.url.touched) {
                <misis-input-error/>
              }
              @if (form().controls.url.errors?.['pattern'] && form().controls.url.touched) {
                <misis-input-error message="Le format de l'url est incorrect."/>
              }
            </misis-input-text>
          </div>
        </fieldset>
      </div>
      <div class="fr-grid-row">
        <div [class.fr-col-offset-md-3]="centerInPage()" class="fr-col-md-6" formArrayName="groupOfPages">
          @for (groupOfPagesForm of form().controls.groupOfPages.controls; track $index) {
            @if ($index === currentGroupOfPagesIndex) {
              <!--START-->
                <!--@if (form().controls.groupOfPages.controls.at(currentGroupOfPagesIndex); as groupOfPagesForm) {-->
              <fieldset class="fr-fieldset" [formGroup]="groupOfPagesForm">
                <legend class="fr-fieldset__legend">
                  <h6>
                    L'organisation de vos pages
                  </h6>
                </legend>
                <div class="fr-fieldset__element">
                  <p class="fr-hint-text">
                    Sauf mention contraire, tous les champs sont obligatoires.
                  </p>
                </div>

                <!-- TODO : L'ancre du lien ne scroll pas au début du formulaire -->
                <div class="fr-fieldset__element" id="creation-d-un-groupe-de-page">
                  <!-- TODO : erreur dans la console du navigateur à la création d'un nouveau groupe -->
                  <misis-input-text
                    label="Nom du groupe"
                    hint="Ce nom sera affiché dans la carte du tableau de suivi"
                    formControlName="name"
                    [required]="true">
                    @if (groupOfPagesForm.controls.name.touched) {
                      @if (groupOfPagesForm.controls.name.errors?.['required']) {
                        <misis-input-error/>
                      }
                      @if (groupOfPagesForm.controls.name.errors?.['pattern']) {
                        <misis-input-error message="Ce champ ne doit pas être vide."/>
                      }
                    }
                  </misis-input-text>
                </div>

                <div class="fr-fieldset__element">
                  <div class="fr-input-group">
                    <div class="fr-select-group"
                         [class.fr-select-group--valid]="groupOfPagesForm.controls.periodiciteDuSuivi.touched && groupOfPagesForm.controls.periodiciteDuSuivi.valid"
                         [class.fr-select-group--error]="groupOfPagesForm.controls.periodiciteDuSuivi.touched && groupOfPagesForm.controls.periodiciteDuSuivi.invalid"
                    >
                      <label class="fr-label" [for]="'periodicite-suivi-' + currentGroupOfPagesIndex">
                        Périodicité du suivi
                      </label>
                      <select class="fr-select" [id]="'periodicite-suivi-' + currentGroupOfPagesIndex" name="periodicite-suivi" required
                              formControlName="periodiciteDuSuivi"
                      >
                        <option value="" selected disabled hidden>
                          Sélectionner une option
                        </option>
                        <option value="QUOTIDIEN">
                          Quotidien
                        </option>
                        <option value="HEBDOMADAIRE">
                          Hebdomadaire
                        </option>
                        <option value="MENSUEL">
                          Mensuel
                        </option>
                      </select>
                      @if (groupOfPagesForm.controls.periodiciteDuSuivi.touched && groupOfPagesForm.controls.periodiciteDuSuivi.errors?.['required']) {
                        <misis-input-error/>
                      }
                    </div>
                  </div>
                </div>

                <div class="fr-fieldset__element">
                  <div class="fr-input-group">
                    <div class="fr-select-group"
                         [class.fr-select-group--valid]="groupOfPagesForm.controls.methodeDeCreationDeGroupe.touched && groupOfPagesForm.controls.methodeDeCreationDeGroupe.valid"
                         [class.fr-select-group--error]="groupOfPagesForm.controls.methodeDeCreationDeGroupe.touched && groupOfPagesForm.controls.methodeDeCreationDeGroupe.invalid"
                    >
                      <label class="fr-label" [for]="'methode-creation-' + currentGroupOfPagesIndex">
                        Méthode de création de groupe
                      </label>
                      <select class="fr-select" [id]="'methode-creation-' + currentGroupOfPagesIndex" name="methode-creation" required
                              formControlName="methodeDeCreationDeGroupe">
                        <option value="" selected disabled hidden>
                          Sélectionner une option
                        </option>
                        <option value="MANUELLE">
                          Manuelle
                        </option>
                        <option disabled>
                          Lien à partir d'une page parente (WIP)
                        </option>
                        <option disabled>
                          Analyse du sitemap (WIP)
                        </option>
                        <option disabled>
                          Par API (WIP)
                        </option>
                      </select>
                      @if (groupOfPagesForm.controls.methodeDeCreationDeGroupe.touched && groupOfPagesForm.controls.methodeDeCreationDeGroupe.errors?.['required']) {
                        <misis-input-error/>
                      }
                    </div>
                  </div>
                </div>
              </fieldset>

              @if (groupOfPagesForm.controls.methodeDeCreationDeGroupe.value === 'MANUELLE') {
                <fieldset class="fr-fieldset" [formGroup]="groupOfPagesForm">
                  <legend class="fr-fieldset__legend">
                    <h6>
                      Saisie des URL des pages
                    </h6>
                  </legend>
                  @if (this.form().controls.url.value; as domain) {
                    <div class="fr-fieldset__element">
                      <p class="fr-hint-text">
                        Sauf mention contraire, tous les champs sont obligatoires.
                      </p>
                      <p class="fr-hint-text">
                        Saisissez une url valide, commençant par https://{{ domain }}
                      </p>
                    </div>
                    <div class="fr-table fr-col-12">
                      <div class="fr-table__wrapper">
                        <div class="fr-table__container">
                          <div class="fr-table__content">
                            <table class="fr-cell--multiline">
                              <caption [hidden]="true">
                                URL des pages du groupe
                              </caption>
                              <thead>
                              <tr>
                                <th scope="col" class="required">
                                  URL
                                </th>
                                <th scope="col" class="fr-cell--right fr-col-1">
                                  Actions
                                </th>
                              </tr>
                              </thead>
                              <tbody>
                                @for (pagesGroup of groupOfPagesForm.controls.pages.controls; track $index) {
                                  <tr id="table-sm-row-key-{{$index}}" [formGroup]="pagesGroup">
                                    <td>
                                      <misis-input-text formControlName="url"
                                                        [placeholder]="'/exemple'"
                                                        [required]="true"
                                                        (keydownEnter)="addPage($event)"
                                      >
                                        @if (pagesGroup.controls['url'].errors?.['required'] && pagesGroup.controls['url'].touched) {
                                          <misis-input-error/>
                                        }
                                        @if (pagesGroup.controls['url'].errors?.['pattern'] && pagesGroup.controls['url'].touched) {
                                          <misis-input-error message="Le format de l'url est incorrect."/>
                                        }
                                      </misis-input-text>
                                    </td>
                                    <td class="fr-cell--right">
                                      <button class="fr-btn fr-btn--tertiary fr-btn--sm fr-icon-delete-fill" title="Supprimer l'URL" (click)="removePage($index, $event)">
                                        <span class="sr-only" rel="noopener noreferrer">Supprimer</span>
                                      </button>
                                    </td>
                                  </tr>
                                }
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="fr-fieldset__element">
                      <ul class="fr-btns-group fr-btns-group--inline fr-btns-group--sm fr-btns-group--right">
                        <li>
                          <button class="fr-btn fr-btn--secondary fr-btns-group--sm" (click)="addPage($event)" title="Ajouter une page à analyser">
                            Ajouter une page
                          </button>
                        </li>
                      </ul>
                    </div>
                  } @else {
                    <misis-input-error message="Merci de saisir le 'Nom de domaine' avant de continuer"/>
                  }
                </fieldset>
              }
            }
          }

          <fieldset class="fr-fieldset">
            <div class="fr-fieldset__element">
              <ul class="fr-btns-group fr-btns-group--inline fr-btns-group--right">
                @if (enableStepBackButton()) {
                  <li>
                    <button class="fr-btn fr-btn--secondary" (click)="processToBackStep($event)">
                      Précédent
                    </button>
                  </li>
                }
                <li>
                  <button class="fr-btn fr-btn--secondary" (click)="addGroup($event)">
                    Créer un autre groupe
                  </button>
                </li>
                <li>
                  <button type="submit" class="fr-btn" (click)="processToNextStep($event)" [disabled]="disableSubmitButton()">
                    Terminer
                  </button>
                </li>
              </ul>
            </div>
          </fieldset>
        </div>
        @if (form().controls.groupOfPages.length > 1) {
          <div class="fr-col">
            <nav class="fr-sidemenu fr-sidemenu--right" aria-labelledby="fr-sidemenu-title">
              <div class="fr-sidemenu__inner">
                <button type="button" class="fr-sidemenu__btn" hidden aria-controls="fr-sidemenu-wrapper" aria-expanded="false">
                  Dans cette rubrique
                </button>
                <div class="fr-collapse" id="fr-sidemenu-wrapper">
                  <div class="fr-sidemenu__title" id="fr-sidemenu-title">
                    Groupe de page créé
                  </div>
                  <ul class="fr-sidemenu__list">
                    @for (group of form().controls.groupOfPages.controls; track $index) {
                      @if (group.controls.name.valid) {
                        <li class="fr-sidemenu__item">
                          <button type="button" class="fr-sidemenu__btn" (click)="switchTo($index)"
                                  [class.fr-icon-error-fill]="group.invalid"
                                  [class.fr-text-label--red-marianne]="group.invalid"
                          >
                            {{ group.controls.name.value }}
                          </button>
                        </li>
                      }
                    }
                  </ul>
                </div>
              </div>
            </nav>
          </div>
        }
      </div>
    </form>
  `,
  styles: ``
})
export class CreationStep2Component {
  centerInPage = input<boolean>(true);
  enableStepBackButton = input<boolean>(true);
  form = input.required<CreationStep2Form>();
  processToStep = output<number>();
  currentGroupOfPagesIndex:number = 0;
  disableSubmitButton = signal<boolean>(false);

  processToNextStep = (event:Event) => {
    event.preventDefault();
    this.form().markAllAsTouched();

    if (this.form().valid) {
      this.disableSubmitButton.set(true);
      this.processToStep.emit(3)
    }
  }

  processToBackStep = (event:Event) => {
    event.preventDefault();
    this.setDefaultName();
    this.processToStep.emit(1)
  }

  addPage(event:Event) {
    event.preventDefault()
    if (this.form().controls.groupOfPages.valid) {
      this.form().controls.groupOfPages.at(this.currentGroupOfPagesIndex).controls.pages.push(buildPagesFormGroup());
    }
  }

  removePage(index: number, event:Event) {
    event.preventDefault()
    const group =this.form().controls.groupOfPages.at(this.currentGroupOfPagesIndex);
    if (group.controls.pages.length === 1) {
      group.controls.pages.at(index).reset();
    } else {
      group.controls.pages.removeAt(index);
    }
    group.controls.pages.markAllAsTouched();
  }

  addGroup = (event:Event) => {
    event.preventDefault()
    if (this.form().valid) {
      this.currentGroupOfPagesIndex=this.form().controls.groupOfPages.length;
      const group = groupCreationForm();
      this.form().controls.groupOfPages.push(group);
      group.markAsUntouched();
    }
  }

  switchTo = (index: number)=> {
    this.setDefaultName();
    this.currentGroupOfPagesIndex=index;
  }

  setDefaultName = (): void => {
    if (this.form().controls.groupOfPages.at(this.currentGroupOfPagesIndex).controls.name.invalid) {
      this.form().controls.groupOfPages.at(this.currentGroupOfPagesIndex).controls.name.setValue(`Groupe de pages n°${this.currentGroupOfPagesIndex+1}`);
    }
  }
}
