import { Component, inject, input } from '@angular/core';
import { SuiviDeSiteStore } from "../../suivi-de-site.store";
import { ConvertBytesToPipe } from "../../../../../../shared/pipes/convert-bytes-to.pipe";
import { ActivatedRoute } from "@angular/router";
import { WebsiteDetailsResourceService } from "../../../../../../rest/api/website-details-resource.service";
import { map, Observable, shareReplay, switchMap } from "rxjs";
import { AsyncPipe } from "@angular/common";
import { ChartsUnit } from "../../../../../../shared/components/line-chart/charts-unit";
import { LineStyle } from "../../../../../../shared/components/line-chart/line-style";
import { LoadingAlertComponent } from '../../../../../../shared/components/loading-alert/loading-alert.component';
import { AlertType } from '../../../../../../shared/components/loading-alert/alert-type';
import { DownloadComponent } from "../../../../../../shared/components/download/download.component";
import { WebsitePagesDetailsDto } from "../../../../../../rest/model/website-pages-details-dto";
import { AnalyseStatut } from "../../../../../../rest/model/AnalyseStatut";

@Component({
  selector: 'misis-liste-des-pages',
  standalone: true,
  imports: [
    ConvertBytesToPipe,
    AsyncPipe,
    LoadingAlertComponent,
    DownloadComponent
  ],
  template: `
    <h2 class="fr-my-2w">Liste des pages</h2>
    <div>
      <div class="fr-editor">
        <div class="fr-grid-row">
          <div class="fr-col">
            <p>
              Les résultats de la dernière analyse représentent le poids des données transférées et décodées au chargement de la page, ainsi que le nombre de requêtes identifiées sur un affichage, et enfin le nombre d'éléments du DOM qui constituent son interface.
            </p>
          </div>
          @if (this.websitePagesDetails$ | async; as websitePagesDetails) {
            <misis-download fileSuffix="Liste des pages" [data]="websitePagesDetails.pageDetailDtos"/>
          }
        </div>
        @if (this.websitePagesDetails$ | async; as website) {
          <div class="fr-table--lg fr-table fr-mt-3w fr-col-12">
            <div class="fr-table__wrapper">
              <div class="fr-table__container">
                <div class="fr-table__content">
                  <table class="fr-cell--multiline">
                    <caption [hidden]="true">
                      Pages du groupe
                    </caption>
                    <thead>
                    <tr>
                      <th scope="col">Lien de la page</th>
                      <th scope="col" class="fr-cell--right">Données transférées</th>
                      <th scope="col" class="fr-cell--right">Données décodées</th>
                      <th scope="col" class="fr-cell--right">Requêtes</th>
                      <th scope="col" class="fr-cell--right">Nombre d'éléments du DOM</th>
                      <th scope="col" class="fr-cell--right">Statut</th>
                    </tr>
                    </thead>
                    <tbody>
                      @for (page of website.pageDetailDtos; track page.url) {
                        <tr>
                        <td>
                            <a [href]="page.url" target="_blank" class="fr-link" title="Accéder à la page - nouvelle fenêtre"
                              [class.fr-ellipsis]="!page.title"
                              [class.link-ellipsis]="!page.title"
                            >
                              {{ page.title ? page.title : page.url }}
                            </a>
                          </td>
                          <td class="fr-cell--right">{{ page.dataTransferedInBytes! | convertBytesTo: 'MB' }}</td>
                          <td class="fr-cell--right">{{ page.dataDecodedInBytes! | convertBytesTo: 'MB' }}</td>
                          <td class="fr-cell--right">{{ page.httpRequestsCount }}</td>
                          <td class="fr-cell--right">{{ page.domElementsCount }}</td>
                          <td class="fr-cell--right">
                            @switch (page.analyseStatut) {
                              @case (AnalyseStatut.ERREUR_PAGE_302) {
                                La page est redirigée (302)
                              }
                              @case (AnalyseStatut.ERREUR_PAGE_404) {
                                La page est introuvable (404)
                              }
                              @case (AnalyseStatut.ERREUR_PAGE_500) {
                                La page contient une erreur interne (500)
                              }
                              @case (AnalyseStatut.INVALIDE) {
                                Une erreur est survenue lors de l’analyse. Veuillez contacter l’administrateur de MISIS
                              }
                              @case (AnalyseStatut.ERREUR_URL_NON_RESOLUE) {
                                Une erreur est survenue en raison du nom de domaine de suivi du site
                              }
                              @case (AnalyseStatut.ERREUR_URL_MALFORMEE || AnalyseStatut.ERREUR_URL_VIDE) {
                                Une erreur est survenue en raison du format de l’URL
                              }
                              @case (AnalyseStatut.NOUVEAU) {
                                En file d’attente pour la première analyse
                              }
                              @case (AnalyseStatut.TERMINEE) {
                                Terminée
                              }
                            }
                          </td>
                        </tr>
                      }
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        } @else {
          <misis-loading-alert [text]="AlertType.LOADING.text" [class]="AlertType.LOADING.class" />
        }
      </div>
    </div>
  `,
  styles: ``
})
export default class ListeDesPagesComponent {
  suiviDeSiteStore = inject(SuiviDeSiteStore);
  id = input<string>();
  websiteDetailsResourceService: WebsiteDetailsResourceService = inject(WebsiteDetailsResourceService)
  activatedRoute = inject(ActivatedRoute);
  suiviDeSiteId: number = this.suiviDeSiteStore.websiteId;

  websitePagesDetails$: Observable<WebsitePagesDetailsDto> = this.activatedRoute.params.pipe(
    map(params => Number(params['id'])),
    switchMap(id => this.websiteDetailsResourceService.suivisDeSiteSuiviIdGroupeGroupIdListeDesPagesGet(id, this.suiviDeSiteId)),
    shareReplay(1)
  )

  protected readonly ChartsUnit = ChartsUnit;
  protected readonly LineStyle = LineStyle;
  protected readonly AlertType = AlertType;
  protected readonly AnalyseStatut = AnalyseStatut;
}
