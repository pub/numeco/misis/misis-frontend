import {Component, inject, input} from '@angular/core';
import {SuiviDeSiteStore} from "../../suivi-de-site.store";
import {ConvertBytesToPipe} from "../../../../../../shared/pipes/convert-bytes-to.pipe";
import {map, Observable, shareReplay, switchMap, tap} from "rxjs";
import {WebsiteDetailsResourceService} from "../../../../../../rest/api/website-details-resource.service";
import {ActivatedRoute} from "@angular/router";
import {RessourceType} from "../../../../../../rest/model/ressource-type";
import {WebsiteResourcesDetailsDto} from "../../../../../../rest/model/website-resources-details-dto";
import {AsyncPipe} from "@angular/common";
import {LineChartComponent} from "../../../../../../shared/components/line-chart/line-chart.component";
import {ChartsUnit} from "../../../../../../shared/components/line-chart/charts-unit";
import {LineStyle} from "../../../../../../shared/components/line-chart/line-style";
import {DataType} from "../../../../../../shared/components/line-chart/data-type";
import {StatisticsResourceService} from '../../../../../../rest/api/statistics-resource.service';
import {StatisticsDto} from '../../../../../../rest/model/statistics-dto';
import {StatisticType} from '../model/statistic-type.enums';
import {LoadingAlertComponent} from '../../../../../../shared/components/loading-alert/loading-alert.component';
import {AlertType} from '../../../../../../shared/components/loading-alert/alert-type';
import {DownloadComponent} from "../../../../../../shared/components/download/download.component";

@Component({
  selector: 'misis-images',
  standalone: true,
  imports: [
    ConvertBytesToPipe,
    AsyncPipe,
    LineChartComponent,
    LoadingAlertComponent,
    DownloadComponent
  ],
  template: `
    <h2 class="fr-my-2w">Images</h2>
    <div>
      <div class="fr-editor">
        <div class="fr-grid-row">
          <div class="fr-col">
            <p>
              Bien que les images de haute qualité puissent sembler être la meilleure option pour améliorer l’apparence d’une plateforme,
              leur utilisation excessive peut avoir des conséquences négatives sur la performance, l’expérience utilisateur et l’environnement.
            </p>
          </div>
          @if (this.websiteResourcesDetails$ | async; as websiteResourcesDetails) {
            <misis-download fileSuffix="Images" [data]="websiteResourcesDetails.resourceDetailDtos"/>
          }
        </div>
        @if (websiteResourcesDetails$ | async; as websiteResourcesDetails) {
          <div class="fr-tabs">
            <ul class="fr-tabs__list" role="tablist" aria-label="Affichage des données concernant les images">
              <li role="presentation">
                <button button id="tabpanel-404" class="fr-tabs__tab fr-icon-checkbox-line fr-tabs__tab--icon-left" tabindex="0" role="tab" aria-selected="true" aria-controls="tabpanel-404-panel">Graphique</button>
              </li>
              <li role="presentation">
                <button id="tabpanel-405" class="fr-tabs__tab fr-icon-checkbox-line fr-tabs__tab--icon-left" tabindex="-1" role="tab" aria-selected="false" aria-controls="tabpanel-405-panel">Liste des images</button>
              </li>
            </ul>
            <div id="tabpanel-404-panel" class="fr-tabs__panel fr-tabs__panel--selected" role="tabpanel" aria-labelledby="tabpanel-404" tabindex="0">
              @if (statistics$ | async; as stats) {
                @if (!stats.datasets || stats.datasets.length == 0) {
                  <misis-loading-alert [text]="AlertType.NO_DATA.text" [class]="AlertType.NO_DATA.class"/>
                } @else if (stats.labels!.length == 0) {
                  <misis-loading-alert [text]="AlertType.FIRST_ANALYZIS.text" [class]="AlertType.FIRST_ANALYZIS.class"/>
                } @else {
                  {{ setThresholdTitle(stats) }}
                  <misis-line-chart
                    [unit]="ChartsUnit.DAY"
                    [data]="stats"
                    [enableDateSelector]="true"
                    [lineStyle]="[LineStyle.pink]"
                    [dataType]="DataType.NUMBER"
                    [title]="title"
                    [subtitle]="subtitle"
                    [hideSwitchInfo]="true"
                  />
                }
              } @else {
                <misis-loading-alert [text]="AlertType.NO_DATA.text" [class]="AlertType.NO_DATA.class"/>
              }
              <div class="fr-grid-row">
                <div class="fr-col">
                  <h3 class="fr-my-2w">Comment réduire le poids des images</h3>
                </div>
              </div>
              <div class="fr-grid-row">
                <div class="fr-col">
                  <p>
                    Il est important de trouver un équilibre entre la qualité des images et la performance globale de la plateforme.
                  </p>
                </div>
              </div>
              <div class="fr-grid-row">
                <div class="fr-col">
                  <p>
                    Il existe différents formats d'images préconisés :
                  </p>
                </div>
              </div>
              <ul>
                <li><b>JPEG</b> : format image de type “photographie” sans effet de transparence souhaité. Ce format est recommandé pour les photos, les dégradés et autres images avec beaucoup de couleurs</li>
                <li><b>PNG</b> : format d’images de type “dessin numérique” avec ou non un effet de transparence. Ce format est recommandé pour les images avec un arrière-fond clair et des illustrations avec peu de couleurs.</li>
                <li><b>SVG</b> : format vectoriel, pour les images de type « dessin numérique ». À privilégier si le poids du SVG est inférieur à celui du PNG généré et si le redimensionnement sans perte est un impératif.</li>
                <li><b>WebP et AVIF</b> : pour les images de type « photographie » et « dessin numérique », dès lors que la compression permet d’obtenir des médias de poids inférieur au JPEG et PNG.</li>
              </ul>
              <div class="fr-grid-row">
                <div class="fr-col">
                  <p>
                    Pour le traitement d’images, différentes solutions d'outils gratuits de compression et de redimensionnement sont faciles à l’emploi et sont préconisés dans le cadre de l’éco-conception.
                  </p>
                </div>
              </div>
              <a href="https://spote.developpement-durable.gouv.fr/interministeriel/plateforme-spote-66/article/eco-conception-spote-utilisation-des-images" target="_blank" title="Article Spote - Utilisation des images en éco-conception - nouvelle fenêtre">Article Spote - Utilisation des images en éco-conception</a>
            </div>
            <div id="tabpanel-405-panel" class="fr-tabs__panel" role="tabpanel" aria-labelledby="tabpanel-405" tabindex="0">
              <div class="fr-table fr-mt-3w fr-col-12">
                <div class="fr-table__wrapper">
                  <div class="fr-table__container">
                    <div class="fr-table__content">
                      <table class="fr-cell--multiline">
                        <caption>
                          Liste des images
                        </caption>
                        <thead>
                        <tr>
                          <th scope="col">Nom de l'image</th>
                          <th scope="col">Lien de la page</th>
                          <th scope="col" class="fr-cell--right">Poids</th>
                        </tr>
                        </thead>
                        <tbody>
                          @for (image of websiteResourcesDetails.resourceDetailDtos; track image.url) {
                            <tr>
                              <td><a [href]="image.url" target="_blank" class="fr-link" title="{{image.filename}} - nouvelle fenêtre">{{ image.filename }}</a></td>
                              <td><a [href]="image.pageUrl" target="_blank" class="fr-link fr-icon-arrow-right-line fr-link--icon-right fr-mt-auto fr-mr-0" title="Page contenant l'image - nouvelle fenêtre">{{ image.pageUrl }}</a></td>
                              <td class="fr-cell--right">{{ image.sizeInBytes! | convertBytesTo: 'KB' }}</td>
                            </tr>
                          } @empty {
                            <tr>
                              <td colspan="3">Aucune image n'a été trouvée.</td>
                            </tr>
                          }
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        } @else {
          <misis-loading-alert [text]="AlertType.LOADING.text" [class]="AlertType.LOADING.class"/>
        }
      </div>
    </div>
  `,
  styles: ``
})
export default class ImagesComponent {
  suiviDeSiteStore = inject(SuiviDeSiteStore);
  id = input<string>();
  websiteDetailsResourceService : WebsiteDetailsResourceService = inject(WebsiteDetailsResourceService);
  statisticsResourceService: StatisticsResourceService = inject(StatisticsResourceService);
  activatedRoute = inject(ActivatedRoute);
  suiviDeSiteId: number = this.suiviDeSiteStore.websiteId;
  title = "";
  subtitle = "Courbe représentative du nombre d'images dépassant le seuil défini sur l'ensemble des pages analysées";

  websiteResourcesDetails$: Observable<WebsiteResourcesDetailsDto> =
    this.activatedRoute.params.pipe(
      map((params) => Number(params['id'])),
      switchMap((id) =>
        this.websiteDetailsResourceService.suivisDeSiteSuiviIdGroupeGroupIdListeDesRessourcesGet(
          id,
          this.suiviDeSiteId,
          RessourceType.IMAGE
        )
      ),
      shareReplay(1)
    );

  statistics$: Observable<StatisticsDto> = this.activatedRoute.params.pipe(
    map((params) => Number(params['id'])),
    switchMap((id) =>
      this.statisticsResourceService.suivisDeSiteSuiviIdGroupeGroupIdStatistiquesStatTypeGet(
        id,
        StatisticType.IMAGE,
        this.suiviDeSiteId
      )
    ),
    tap(stats => stats.datasets!.reverse())
  );

  setThresholdTitle(stats: StatisticsDto) {
    let threshold: number = 100000;
    if (stats.datasets![1].data) {
      threshold = stats.datasets![1].data[stats.datasets![1].data.length - 1];
    }
    this.title = "Nombre d'images supérieures à " + threshold.toLocaleString(undefined, DataType.BYTE);
  }

  protected readonly ChartsUnit = ChartsUnit;
  protected readonly LineStyle = LineStyle;
  protected readonly DataType = DataType;
  protected readonly AlertType = AlertType;
}
