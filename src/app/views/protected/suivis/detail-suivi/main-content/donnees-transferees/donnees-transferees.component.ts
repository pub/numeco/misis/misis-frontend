import {Component, inject, input} from '@angular/core';
import {SuiviDeSiteStore} from "../../suivi-de-site.store";
import {ActivatedRoute} from "@angular/router";
import {map, Observable, shareReplay, switchMap} from "rxjs";
import {AsyncPipe} from "@angular/common";
import {LineChartComponent} from "../../../../../../shared/components/line-chart/line-chart.component";
import {ChartsUnit} from "../../../../../../shared/components/line-chart/charts-unit";
import {LineStyle} from "../../../../../../shared/components/line-chart/line-style";
import {DataType} from "../../../../../../shared/components/line-chart/data-type";
import {StatisticsResourceService} from '../../../../../../rest/api/statistics-resource.service';
import {StatisticsDto} from '../../../../../../rest/model/statistics-dto';
import {StatisticType} from '../model/statistic-type.enums';
import {LoadingAlertComponent} from '../../../../../../shared/components/loading-alert/loading-alert.component';
import {AlertType} from '../../../../../../shared/components/loading-alert/alert-type';
import {DownloadComponent} from "../../../../../../shared/components/download/download.component";

@Component({
  selector: 'misis-donnees-transferees',
  standalone: true,
  imports: [
    AsyncPipe,
    LineChartComponent,
    LoadingAlertComponent,
    DownloadComponent
  ],
  template: `
    <h2 class="fr-my-2w">Données transférées</h2>
    <div>
      <div class="fr-editor">
        <div class="fr-grid-row">
          <div class="fr-col">
            <p>
              Une partie de l’empreinte énergétique des services numériques est liée à la volumétrie des données échangées sur les réseaux.
            </p>
          </div>
          @if (this.statistics$ | async; as statisticsDto) {
            <misis-download fileSuffix="Données transférées" [data]="statisticsDto.datasets"/>
          }
        </div>
        @if (this.statistics$ | async; as stats) {
          @if (stats.datasets!.length == 0) {
            <misis-loading-alert [text]="AlertType.FIRST_ANALYZIS.text" [class]="AlertType.FIRST_ANALYZIS.class" />
          }
          <misis-line-chart
          [unit]="ChartsUnit.DAY"
          [data]="stats"
          [enableDateSelector]="true"
          [lineStyle]="[LineStyle.dashedBlue, LineStyle.dashedGreen, LineStyle.pink]"
          [dataType]="DataType.BYTE"
          [title]="title"
          [subtitle]="subtitle"
          />
        } @else {
          <misis-loading-alert [text]="AlertType.LOADING.text" [class]="AlertType.LOADING.class" />
        }
        <div class="fr-grid-row">
          <div class="fr-col">
            <h3 class="fr-my-2w">Comment réduire l'impact des données transférées</h3>
          </div>
        </div>
        <div class="fr-grid-row">
          <div class="fr-col">
            <p>
              Voici quelques conseils pour réduire le flux de données :
            </p>
          </div>
        </div>
        <ul>
          <li>Réduire le poids des contenus hébergés sur les serveurs.</li>
          <li>Mettre en cache les données.</li>
          <li>Optimiser les flux en limitant les données au strict nécessaire et en les compressant.</li>
          <li>Réduire la qualité des vidéos, proposer des contenus audio si cela est pertinent.</li>
          <li>Développer le service numérique de telle manière qu'il ne provoque pas le visionnage automatique de vidéos.</li>
          <li>Réduire la volumétrie des données pour le service en évitant de réaliser des requêtes client/serveur inutiles.</li>
          <li>Réduire les appels à des API, scripts, librairies ou polices de caractères tiers.</li>
        </ul>
        <div class="fr-grid-row">
          <div class="fr-col">
            <p>
              Pour aller plus loin :
            </p>
          </div>
        </div>
        <div class="fr-grid-row">
          <div class="fr-col">
            <a href="https://www.arcep.fr/uploads/tx_gspublication/referentiel_general_ecoconception_des_services_numeriques_version_2024.pdf" target="_blank" title="Référentiel Général de l'Écoconception des Services Numériques (RGESN) - Mai 2024 (PDF) - nouvelle fenêtre">Référentiel Général de l'Écoconception des Services Numériques (RGESN) - Mai 2024 (PDF)</a>
          </div>
        </div>
      </div>
    </div>
  `,
  styles: ``
})
export default class DonneesTransfereesComponent {
  suiviDeSiteStore = inject(SuiviDeSiteStore);
  id = input<string>();
  statisticsResourceService: StatisticsResourceService = inject(StatisticsResourceService);
  activatedRoute = inject(ActivatedRoute);
  suiviDeSiteId: number = this.suiviDeSiteStore.websiteId;
  title = "Poids des données transférées";
  subtitle = "Courbe représentative de l'évolution du poids des données transférées sur l'ensemble des pages analysées";

  statistics$: Observable<StatisticsDto> = this.activatedRoute.params.pipe(
    map(params => Number(params['id'])),
    switchMap(id => this.statisticsResourceService.suivisDeSiteSuiviIdGroupeGroupIdStatistiquesStatTypeGet(id, StatisticType.TRANSFERRED_DATA, this.suiviDeSiteId)),
    shareReplay(1)
  )

  protected readonly ChartsUnit = ChartsUnit;
  protected readonly LineStyle = LineStyle;
  protected readonly DataType = DataType;
  protected readonly AlertType = AlertType;
}
