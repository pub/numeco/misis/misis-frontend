import {Component, inject, input} from '@angular/core';
import {ConvertBytesToPipe} from "../../../../../../shared/pipes/convert-bytes-to.pipe";
import {SuiviDeSiteStore} from "../../suivi-de-site.store";
import {WebsiteDetailsResourceService} from "../../../../../../rest/api/website-details-resource.service";
import {ActivatedRoute} from "@angular/router";
import {RessourceType} from "../../../../../../rest/model/ressource-type";
import {map, Observable, shareReplay, switchMap} from "rxjs";
import {WebsiteResourcesDetailsDto} from "../../../../../../rest/model/website-resources-details-dto";
import {AsyncPipe} from "@angular/common";
import {LineChartComponent} from "../../../../../../shared/components/line-chart/line-chart.component";
import {ChartsUnit} from "../../../../../../shared/components/line-chart/charts-unit";
import {LineStyle} from "../../../../../../shared/components/line-chart/line-style";
import {DataType} from "../../../../../../shared/components/line-chart/data-type";
import {StatisticsDto} from '../../../../../../rest/model/statistics-dto';
import {StatisticsResourceService} from '../../../../../../rest/api/statistics-resource.service';
import {StatisticType} from '../model/statistic-type.enums';
import {LoadingAlertComponent} from '../../../../../../shared/components/loading-alert/loading-alert.component';
import {AlertType} from '../../../../../../shared/components/loading-alert/alert-type';
import {DownloadComponent} from "../../../../../../shared/components/download/download.component";

@Component({
  selector: 'misis-documents-pdf',
  standalone: true,
  imports: [
    ConvertBytesToPipe,
    AsyncPipe,
    LineChartComponent,
    LoadingAlertComponent,
    DownloadComponent
  ],
  template: `
    <h2 class="fr-my-2w">Documents PDF</h2>
    <div>
      <div class="fr-editor">
        <div class="fr-grid-row">
          <div class="fr-col">
            <p>
              Les fichiers PDF (Portable Document Format) accompagnent parfois les articles web, en tant que liens ou pièces jointes pour compléter les propos, faciliter les présentations et les échanges de contenus.
              Ils peuvent être consultables depuis le navigateur, et le plus souvent, proposés au téléchargement, avec une notion de poids conséquente.
            </p>
          </div>
          @if (this.websiteResourcesDetails$ | async; as websiteResourcesDetails) {
            <misis-download fileSuffix="Documents PDF" [data]="websiteResourcesDetails.resourceDetailDtos!"/>
          }
        </div>
        @if (websiteResourcesDetails$ | async; as websiteResourcesDetails) {
          <div class="fr-tabs">
            <ul class="fr-tabs__list" role="tablist" aria-label="Affichage des données concernant les images">
              <li role="presentation">
                <button button id="tabpanel-404" class="fr-tabs__tab fr-icon-checkbox-line fr-tabs__tab--icon-left" tabindex="0" role="tab" aria-selected="true" aria-controls="tabpanel-404-panel">Graphique</button>
              </li>
              <li role="presentation">
                <button id="tabpanel-405" class="fr-tabs__tab fr-icon-checkbox-line fr-tabs__tab--icon-left" tabindex="-1" role="tab" aria-selected="false" aria-controls="tabpanel-405-panel">Liste des documents</button>
              </li>
            </ul>
            <div id="tabpanel-404-panel" class="fr-tabs__panel fr-tabs__panel--selected" role="tabpanel" aria-labelledby="tabpanel-404" tabindex="0">
              @if (statistics$ | async; as stats) {
                @if (websiteResourcesDetails.resourceDetailDtos && websiteResourcesDetails.resourceDetailDtos.length == 0) {
                  <misis-loading-alert [text]="AlertType.NO_DATA.text" [class]="AlertType.NO_DATA.class" />
                } @else if (stats.labels!.length == 0) {
                  <misis-loading-alert [text]="AlertType.FIRST_ANALYZIS.text" [class]="AlertType.FIRST_ANALYZIS.class" />
                } @else {
                  <misis-line-chart
                    [unit]="ChartsUnit.DAY"
                    [data]="stats"
                    [lineStyle]="[LineStyle.dashedBlue, LineStyle.dashedGreen, LineStyle.pink]"
                    [enableDateSelector]="true"
                    [dataType]="DataType.BYTE"
                    [title]="title"
                    [subtitle]="subtitle"
                  />
                }
              } @else {
                <misis-loading-alert [text]="AlertType.NO_DATA.text" [class]="AlertType.NO_DATA.class" />
              }
              <div class="fr-grid-row">
                <div class="fr-col">
                  <h3 class="fr-my-2w">Comment réduire le poids des documents PDF</h3>
                </div>
              </div>
              <div class="fr-grid-row">
                <div class="fr-col">
                  <p>
                    Pour réduire le poids d'un document PDF, il y a la possibilité de recourir à l'outil gratuit de compression en ligne d'Adobe Acrobat. Celui-ci peut compresser des fichiers PDF pesant jusqu’à 2 Go.
                  </p>
                </div>
              </div>
              <div class="fr-grid-row">
                <div class="fr-col">
                  <p>
                    L’outil de compression de PDF permet de compresser les fichiers volumineux de ce format directement dans le navigateur, ainsi que de définir une taille de fichier optimale qui ne nuira pas à la qualité de vos images, polices et autres contenus.
                  </p>
                </div>
              </div>
              <div class="fr-grid-row">
                <div class="fr-col">
                  <a href="https://www.adobe.com/fr/acrobat/online/compress-pdf.html" target="_blank" title="Outil de compression en ligne d'Adobe Acrobat (gratuit) - nouvelle fenêtre">Outil de compression en ligne d'Adobe Acrobat (gratuit)</a>
                </div>
              </div>
            </div>
            <div id="tabpanel-405-panel" class="fr-tabs__panel" role="tabpanel" aria-labelledby="tabpanel-405" tabindex="0">
              <div class="fr-table fr-mt-3w fr-col-12">
                <div class="fr-table__wrapper">
                  <div class="fr-table__container">
                    <div class="fr-table__content">
                      <table class="fr-cell--multiline">
                        <caption>
                          Liste des documents PDF
                        </caption>
                        <thead>
                        <tr>
                          <th scope="col">Nom du document</th>
                          <th scope="col">Lien de la page</th>
                          <th scope="col" class="fr-cell--right">Poids</th>
                        </tr>
                        </thead>
                        <tbody>
                          @for (pdf of websiteResourcesDetails.resourceDetailDtos; track pdf.url) {
                            <tr>
                              <td><a [href]="pdf.url" target="_blank" class="fr-link" title="{{pdf.filename}} - nouvelle fenêtre">{{ pdf.filename }}</a></td>
                              <td><a [href]="pdf.pageUrl" target="_blank" class="fr-link" title="Page contenant le document - nouvelle fenêtre">{{ pdf.pageUrl }}</a></td>
                              <td class="fr-cell--right">{{ pdf.sizeInBytes! | convertBytesTo: 'MB' }}</td>
                            </tr>
                          } @empty {
                            <tr>
                              <td colspan="3">Aucun document PDF n'a été trouvé.</td>
                            </tr>
                          }
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        } @else {
          <misis-loading-alert [text]="AlertType.LOADING.text" [class]="AlertType.LOADING.class" />
        }
      </div>
    </div>
  `,
  styles: ``
})
export default class DocumentsPdfComponent {
  suiviDeSiteStore = inject(SuiviDeSiteStore);
  id = input.required<string>();
  websiteDetailsResourceService : WebsiteDetailsResourceService = inject(WebsiteDetailsResourceService);
  statisticsResourceService : StatisticsResourceService = inject(StatisticsResourceService);
  activatedRoute = inject(ActivatedRoute);
  suiviDeSiteId: number = this.suiviDeSiteStore.websiteId;
  title = "Poids des documents PDF";
  subtitle = "Courbe représentative de l'évolution du poids des documents PDF sur l'ensemble des pages analysées";

  statistics$: Observable<StatisticsDto> = this.activatedRoute.params.pipe(
    map((params) => Number(params['id'])),
    switchMap((id) =>
      this.statisticsResourceService.suivisDeSiteSuiviIdGroupeGroupIdStatistiquesStatTypeGet(
        id,
        StatisticType.PDF,
        this.suiviDeSiteId
      )
    )
  );

  websiteResourcesDetails$: Observable<WebsiteResourcesDetailsDto> =
    this.activatedRoute.params.pipe(
      map((params) => Number(params['id'])),
      switchMap((id) =>
        this.websiteDetailsResourceService.suivisDeSiteSuiviIdGroupeGroupIdListeDesRessourcesGet(
          id,
          this.suiviDeSiteId,
          RessourceType.PDF
        )),
      shareReplay(1)
    );

  protected readonly ChartsUnit = ChartsUnit;
  protected readonly LineStyle = LineStyle;
  protected readonly DataType = DataType;
  protected readonly AlertType = AlertType;
}
