import {Component, inject, input} from '@angular/core';
import {SuiviDeSiteStore} from "../../suivi-de-site.store";
import {ActivatedRoute} from "@angular/router";
import {map, Observable, shareReplay, switchMap} from "rxjs";
import {AsyncPipe} from "@angular/common";
import {LineChartComponent} from "../../../../../../shared/components/line-chart/line-chart.component";
import {ChartsUnit} from "../../../../../../shared/components/line-chart/charts-unit";
import {LineStyle} from "../../../../../../shared/components/line-chart/line-style";
import {DataType} from "../../../../../../shared/components/line-chart/data-type";
import {StatisticsResourceService} from '../../../../../../rest/api/statistics-resource.service';
import {StatisticsDto} from '../../../../../../rest/model/statistics-dto';
import {StatisticType} from '../model/statistic-type.enums';
import {LoadingAlertComponent} from '../../../../../../shared/components/loading-alert/loading-alert.component';
import {AlertType} from '../../../../../../shared/components/loading-alert/alert-type';
import {DownloadComponent} from "../../../../../../shared/components/download/download.component";


@Component({
  selector: 'misis-elements-dom',
  standalone: true,
  imports: [
    AsyncPipe,
    LineChartComponent,
    LoadingAlertComponent,
    DownloadComponent
  ],
  template: `
    <h2 class="fr-my-2w">Nombre d'éléments du DOM</h2>
    <div>
      <div class="fr-editor">
        <div class="fr-grid-row">
          <div class="fr-col">
            <p>
              Le DOM (Document Object Model) représente de manière structurée la page web. Il hiérarchise le contenu sous forme d'arborescence, composée de nœuds qui définissent les différents éléments de l'interface.
              <br />Ces éléments peuvent avoir un impact de par leur taille ou leur nombre.
            </p>
          </div>
          @if (this.statistics$ | async; as statisticsDto) {
            <misis-download fileSuffix="Nombre d'éléments du DOM" [data]="statisticsDto.datasets"/>
          }
        </div>
        @if (this.statistics$ | async; as stats) {
          @if (stats.datasets!.length == 0) {
            <misis-loading-alert [text]="AlertType.FIRST_ANALYZIS.text" [class]="AlertType.FIRST_ANALYZIS.class" />
          }
          <misis-line-chart
          [unit]="ChartsUnit.DAY"
          [data]="stats"
          [enableDateSelector]="true"
          [lineStyle]="[LineStyle.dashedBlue, LineStyle.dashedGreen, LineStyle.pink]"
          [dataType]="DataType.NUMBER"
          [title]="title"
          [subtitle]="subtitle"
          />
        } @else {
          <misis-loading-alert [text]="AlertType.LOADING.text" [class]="AlertType.LOADING.class" />
        }
        <div class="fr-grid-row">
          <div class="fr-col">
            <h3 class="fr-my-2w">Comment réduire le nombre d'éléments du DOM</h3>
          </div>
        </div>
        <div class="fr-grid-row">
          <div class="fr-col">
            <p>
              Voici quelques conseils pour réduire l'impact de la structure DOM :
            </p>
          </div>
        </div>
        <ul>
          <li>Éviter une structure trop complexe.</li>
          <li>Réduire le nombre d'éléments du DOM ou le nombre d'éléments imbriqués (profondeur de l'arborescence).</li>
          <li>Supprimer ou limiter les éléments cachés.</li>
          <li>Veiller au nombre de pugins ajoutant des blocs d'éléments.</li>
          <li>Répartir les informations sur plusieurs pages si cela est pertinent afin d'avoir des interfaces individuelles plus légères.</li>
        </ul>
        <div class="fr-grid-row">
          <div class="fr-col">
            <p>
              Pour aller plus loin :
            </p>
          </div>
        </div>
        <div class="fr-grid-row">
          <div class="fr-col">
            <a href="https://www.arcep.fr/uploads/tx_gspublication/referentiel_general_ecoconception_des_services_numeriques_version_2024.pdf" target="_blank" title="Référentiel Général de l'Écoconception des Services Numériques (RGESN) - Mai 2024 (PDF) - nouvelle fenêtre">Référentiel Général de l'Écoconception des Services Numériques (RGESN) - Mai 2024 (PDF)</a>
          </div>
        </div>
      </div>
    </div>
  `,
  styles: ``
})
export default class ElementsDOMComponent {
  suiviDeSiteStore = inject(SuiviDeSiteStore);
  id = input<string>();
  statisticsResourceService: StatisticsResourceService = inject(StatisticsResourceService);
  activatedRoute = inject(ActivatedRoute);
  suiviDeSiteId: number = this.suiviDeSiteStore.websiteId;
  title = "Nombre d'éléments du DOM";
  subtitle = "Courbe représentative de l'évolution du nombre d'éléments du DOM sur l'ensemble des pages analysées";

  statistics$: Observable<StatisticsDto> = this.activatedRoute.params.pipe(
    map(params => Number(params['id'])),
    switchMap(id => this.statisticsResourceService.suivisDeSiteSuiviIdGroupeGroupIdStatistiquesStatTypeGet(id, StatisticType.DOM_ELEMENTS, this.suiviDeSiteId)),
    shareReplay(1)
  )

  protected readonly ChartsUnit = ChartsUnit;
  protected readonly LineStyle = LineStyle;
  protected readonly DataType = DataType;
  protected readonly AlertType = AlertType;
}
