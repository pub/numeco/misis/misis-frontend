import {Component, input, InputSignal} from '@angular/core';
import {I18nPluralPipe} from "@angular/common";
import {RouterLink} from "@angular/router";
import {WebsiteSummaryDto} from "../../../../../rest/model/website-summary-dto";

@Component({
    selector: 'misis-detail-suivi-header',
    standalone: true,
    imports: [
        I18nPluralPipe,
        RouterLink
    ],
    template: `
        <div class="header">
            <div class="fr-container fr-grid-row">

                @if (this.websiteSummary(); as suiviSite) {
                    <div class="fr-col-md-12 fr-col-lg-4 fr-col-offset-2--right">
                        <h2>{{ suiviSite.name }}</h2>
                        <p>{{ suiviSite.pageCount | i18nPlural : pluralPage }} {{ Object.keys(suiviSite.groups || {}).length | i18nPlural : pluralGroup }}</p>
                    </div>

                    <div class="button-group fr-col-md-12 fr-col-lg-6">
                      <button class="fr-btn fr-btn--secondary" title="Modifier les paramètres du suivi"
                      [routerLink]="['parametrer']" fragment="{{ suiviSite.name }}">
                      <span class="fr-icon-settings-5-line  fr-mr-2v" aria-hidden="true"></span>
                      Paramètres du suivi
                    </button>
                  </div>
                }
            </div>
        </div>
    `,
    styles: `
      .header {
        background-color: var(--background-alt-blue-france);
        padding: 10px 20px;
        font-size: 20px;
        font-weight: 400;

        div {
          align-items: center;
        }
      }

      .button-group {
        display: flex;
        justify-content: flex-end;
      }

      @media screen and (max-width: 991px) {
        .button-group {
          justify-content: flex-start;
        }
      }
    `
})
export class DetailSuiviHeaderComponent {
    websiteSummary: InputSignal<WebsiteSummaryDto | null> = input.required<WebsiteSummaryDto | null>();

    /** Plural maps */
    pluralPage = {
        '=0': 'Aucune page suivie',
        '=1': "Une page suivie",
        other: "# pages suivies"
    };
    pluralGroup = {
        '=0': 'dans aucun groupe',
        '=1': 'dans un groupe',
        other: 'dans # groupes'
    };

    /** Breadcrumb items */
    breadCrumbItems = [
        {label: 'Tableaux de suivi', url: '/tableaux-de-suivi'},
        {label: 'Détail du suivi', url: ''}
    ];

    protected readonly Object = Object;
}
