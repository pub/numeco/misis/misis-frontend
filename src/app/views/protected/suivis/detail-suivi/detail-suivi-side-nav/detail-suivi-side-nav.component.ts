import {Component, input, InputSignal} from '@angular/core';
import {RouterLink, RouterLinkActive} from "@angular/router";
import {KeyValuePipe} from "@angular/common";
import {WebsiteSummaryDto} from "../../../../../rest/model/website-summary-dto";
import {Alert} from "../../../../../rest/model/alert";

@Component({
    selector: 'misis-detail-suivi-side-nav',
    standalone: true,
  imports: [
    RouterLinkActive,
    RouterLink,
    KeyValuePipe
  ],
    template: `
      <nav class="fr-sidemenu fr-sidemenu--sticky-full-height" role="navigation" aria-labelledby="fr-sidemenu-title">
        <div class="fr-sidemenu__inner">
          <button class="fr-sidemenu__btn" aria-controls="fr-sidemenu-wrapper" aria-expanded="false"> Dans cette
            rubrique
          </button>
          <div class="fr-collapse" id="fr-sidemenu-wrapper">
            <ul class="fr-sidemenu__list">

              @if (websiteSummary()) {

                @for (groupe of websiteSummary()?.groups! | keyvalue; track groupe.key) {
                  <li class="fr-sidemenu__item">
                    <button class="fr-sidemenu__btn" [attr.aria-expanded]="rla.isActive"
                            [routerLink]="['groupe', groupe.key]"
                            [attr.aria-controls]="'fr-sidemenu-item-' + groupe.key" routerLinkActive="active" #rla="routerLinkActive"
                            ariaCurrentWhenActive="page">
                      @if (groupe.value.alert) {
                        <span
                          [class.fr-fi-warning-fill]="groupe.value.alert === Alert.WARNING"
                          [class.fr-fi-error-fill]="groupe.value.alert === Alert.ERROR"
                        >&nbsp;</span>
                      }
                      {{ groupe.value.name }}

                    </button>
                    <div class="fr-collapse" id="fr-sidemenu-item-{{groupe.key}}">
                      <ul class="fr-sidemenu__list">
                        <li class="fr-sidemenu__item">
                          <a class="fr-sidemenu__link" [routerLink]="['groupe', groupe.key, 'liste-des-pages']"
                             routerLinkActive="active" ariaCurrentWhenActive="page">Liste des
                            pages</a>
                        </li>
                        <li class="fr-sidemenu__item">
                          <a class="fr-sidemenu__link" [routerLink]="['groupe', groupe.key, 'images']"
                             routerLinkActive="active" ariaCurrentWhenActive="page"
                          >Images</a>
                        </li>
                        <li class="fr-sidemenu__item"
                            [attr.aria-describedby]="groupe.value.availableRessourceTypes?.includes('PDF') ? '': 'tooltip-pdf-'+groupe.key"
                        >
                          @if (!groupe.value.availableRessourceTypes?.includes('PDF')) {
                            <span class="fr-tooltip fr-placement" id="tooltip-pdf-{{groupe.key}}" role="tooltip" aria-hidden="true">
                                Aucun PDF trouvé
                            </span>
                          }
                          <a class="fr-sidemenu__link" [routerLink]="['groupe', groupe.key, 'documents-pdf']"
                             routerLinkActive="active" ariaCurrentWhenActive="page"
                             [attr.disabled]="!groupe.value.availableRessourceTypes?.includes('PDF')"
                          >Documents PDF</a>
                        </li>
                        <li class="fr-sidemenu__item"
                            [attr.aria-describedby]="groupe.value.availableRessourceTypes?.includes('OTHERS') ? '': 'tooltip-others-'+groupe.key"
                        >
                          <span class="fr-tooltip fr-placement" id="tooltip-others-{{groupe.key}}" role="tooltip" aria-hidden="true">
                            Aucun document analysé
                          </span>
                          <a class="fr-sidemenu__link" routerLinkActive="active" ariaCurrentWhenActive="page"
                             [routerLink]="['groupe', groupe.key, 'autres-documents']"
                             [attr.disabled]="!groupe.value.availableRessourceTypes?.includes('OTHERS')"
                          >Autres documents</a>
                        </li>
                        <li class="fr-sidemenu__item"
                            [attr.aria-describedby]="groupe.value.availableRessourceTypes?.includes('STREAMING') ? '': 'tooltip-streaming-'+groupe.key"
                        >
                          <span class="fr-tooltip fr-placement" id="tooltip-streaming-{{groupe.key}}" role="tooltip" aria-hidden="true">
                            Aucun média analysé
                          </span>
                          <a class="fr-sidemenu__link" [routerLink]="['groupe', groupe.key, 'diffusion']"
                             routerLinkActive="active" ariaCurrentWhenActive="page"
                             [attr.disabled]="!groupe.value.availableRessourceTypes?.includes('STREAMING')"
                          >Médias</a>
                        </li>
                        <li class="fr-sidemenu__item">
                          <a class="fr-sidemenu__link" [routerLink]="['groupe', groupe.key, 'donnees-transferees']"
                             routerLinkActive="active" ariaCurrentWhenActive="page">Données transférées</a>
                        </li>
                        <li class="fr-sidemenu__item">
                          <a class="fr-sidemenu__link" [routerLink]="['groupe', groupe.key, 'requetes']"
                             routerLinkActive="active" ariaCurrentWhenActive="page">Requêtes</a>
                        </li>
                        <li class="fr-sidemenu__item">
                          <a class="fr-sidemenu__link" [routerLink]="['groupe', groupe.key, 'elements-dom']"
                             routerLinkActive="active" ariaCurrentWhenActive="page">Éléments du DOM</a>
                        </li>
                      </ul>
                    </div>
                  </li>
                }

              }

            </ul>
          </div>
        </div>
      </nav>
    `,
    styles: `
    a[disabled="true"] {
      pointer-events: none;
      color: gray;
    }
    `
})
export class DetailSuiviSideNavComponent {
    websiteSummary: InputSignal<WebsiteSummaryDto | null> = input.required<WebsiteSummaryDto | null>();
  protected readonly Alert = Alert;
}
