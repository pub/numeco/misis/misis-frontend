import {Component, inject} from '@angular/core';
import {DetailSuiviHeaderComponent} from "./detail-suivi-header/detail-suivi-header.component";
import {DetailSuiviSideNavComponent} from "./detail-suivi-side-nav/detail-suivi-side-nav.component";
import {ActivatedRoute, RouterOutlet} from "@angular/router";
import {SuiviDeSiteStore} from "./suivi-de-site.store";
import {WebsiteResourceService} from "../../../../rest/api/website-resource.service";
import {map, shareReplay, switchMap, tap} from "rxjs";
import {AsyncPipe} from "@angular/common";
import {takeUntilDestroyed} from "@angular/core/rxjs-interop";

@Component({
    selector: 'misis-detail-suivi',
    standalone: true,
    imports: [
        DetailSuiviHeaderComponent,
        DetailSuiviSideNavComponent,
        RouterOutlet,
        AsyncPipe
    ],
    template: `
        <misis-detail-suivi-header [websiteSummary]="summarySuivi$ | async"/>
        <div class="fr-grid-row fr-container">

            <div class="fr-col-12 fr-col-md-4 fr-col-lg-3">
                <misis-detail-suivi-side-nav [websiteSummary]="summarySuivi$ | async"/>
            </div>

            <div class="fr-col-12 fr-col-md-8 fr-col-lg-9 fr-mb-3w">
                <router-outlet></router-outlet>
            </div>

        </div>
    `,
    styles: `
      :host {
        display: block;
        height: 100%;
      }
    `
})
export default class DetailSuiviComponent {

    /** Dependencies */
    private readonly activatedRoute = inject(ActivatedRoute);
    private readonly suiviDeSiteStore: SuiviDeSiteStore = inject(SuiviDeSiteStore);
    private readonly websiteResourceService: WebsiteResourceService = inject(WebsiteResourceService);

    readonly summarySuivi$ = this.activatedRoute.params.pipe(
        map(params => Number(params['id'])),
        tap(id => this.suiviDeSiteStore.setWebsiteId(id)),
        switchMap(id => this.websiteResourceService.suivisDeSiteIdGet(id)),
        takeUntilDestroyed(),
        shareReplay({bufferSize: 1, refCount: true})
    );

}
