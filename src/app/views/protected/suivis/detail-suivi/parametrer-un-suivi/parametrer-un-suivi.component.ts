import {Component, inject, signal} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {map, mergeMap, switchMap, tap} from 'rxjs';
import {GererLesCollaborateursComponent} from './gerer-les-collaborateurs/gerer-les-collaborateurs.component';
import {TableauxDeSuiviService} from '../../tableaux-de-suivi.service';
import {FormArray, FormControl, FormGroup, ReactiveFormsModule, Validators} from '@angular/forms';
import {SupprimerUnSuiviComponent} from './supprimer-un-suivi/supprimer-un-suivi.component';
import {GroupOfPagesFormDto} from "../../../../../rest/model/group-of-pages-form-dto";
import {Constants} from "../../../../../constants";
import {WebsiteSettingsRessourceService} from "../../../../../rest/api/website-settings-ressource.service";
import {WebsiteUserDto} from "../../../../../rest/model/website-user-dto";
import {CreationStep2Form, GroupCreationForm, managePagesOnUrlChanges, step2} from "../../creation-step/creation-step-configuration";
import {CreationStep2Component} from "../../creation-step/creation-step2.component";

@Component({
  selector: 'misis-parametrer-un-suivi',
  standalone: true,
  imports: [
    GererLesCollaborateursComponent,
    SupprimerUnSuiviComponent,
    ReactiveFormsModule,
    CreationStep2Component
  ],
  template: `
    <div class="fr-container" xmlns="http://www.w3.org/1999/html">
      <div id="contenu" class="fr-col-12 fr-py-12v">
        <h1>Paramètres - {{ getSuiviName() }}</h1>
        <div class="fr-grid-row">
          <div class="fr-tabs">
            <ul class="fr-tabs__list" role="tablist" aria-label="Paramétrer le suivi et ses groupes">
              <li role="presentation">
                <button id="tabpanel-404" class="fr-tabs__tab fr-icon-ball-pen-line fr-tabs__tab--icon-left" tabindex="0" role="tab" aria-selected="" aria-controls="tabpanel-404-panel">
                  Gestion du suivi
                </button>
              </li>
              <li role="presentation">
                <button id="tabpanel-405" class="fr-tabs__tab fr-icon-todo-line fr-tabs__tab--icon-left" tabindex="-1" role="tab" aria-selected="" aria-controls="tabpanel-405-panel">
                  Gestion des groupes
                </button>
              </li>
            </ul>
            <div id="tabpanel-404-panel" class="fr-tabs__panel fr-tabs__panel--selected" role="tabpanel" aria-labelledby="tabpanel-404">
              <misis-gerer-les-collaborateurs (onAdd)="ajouterCollaborateur($event)" [users]="users()"/>
              <misis-supprimer-un-suivi/>
            </div>
            <div id="tabpanel-405-panel" class="fr-tabs__panel" role="tabpanel" aria-labelledby="tabpanel-405">
              <div misis-creation-step2 [form]="form" [centerInPage]="false" [enableStepBackButton]="false" (processToStep)="onSubmit()"></div>
<!--              <misis-modifier-les-groupes [form]="form"/>-->
            </div>
          </div>
        </div>
      </div>
    </div>
  `,
  styles: ``
})
export default class ParametrerUnSuiviComponent {
  /** Dependencies */
  private activatedRoute = inject(ActivatedRoute);
  private tableauxDeSuiviService = inject(TableauxDeSuiviService);
  private websiteSettingsRessourceService = inject(WebsiteSettingsRessourceService);
  private router = inject(Router);

  form: CreationStep2Form = step2();
  users = signal<Array<WebsiteUserDto>>([])

  constructor() {
    this.activatedRoute.parent!.params.pipe(
      map(params => params['id']),
      switchMap(id => this.websiteSettingsRessourceService.suivisDeSiteSettingsSuiviIdGet(id)),
      tap(settings => this.users.set(settings.users!)),
      mergeMap(settings => settings.groupOfPagesFormDtos!),
      map(group => this.transformToFormGroup(group)),
    ).subscribe(value => this.form.controls.groupOfPages.push(value));
    this.form.controls.groupOfPages.removeAt(0);
    managePagesOnUrlChanges(this.form);
  }

  private transformToFormGroup(group: GroupOfPagesFormDto) : GroupCreationForm {
    this.setDomainFormDto(group);
    return new FormGroup({
      id: new FormControl(group.id!),
      name: new FormControl(group.name),
      periodiciteDuSuivi: new FormControl(group.periodiciteDuSuivi.toString()),
      methodeDeCreationDeGroupe: new FormControl(group.methodeDeCreationDeGroupe.toString()),
      pages: new FormArray(group.pages!.map(page => new FormGroup({
        id: new FormControl(page.id),
        url: new FormControl(this.getPathFromUrl(page.url!), [Validators.required, Validators.pattern(Constants.PATH_PATTERN)]),
      }))),
    }) as GroupCreationForm;
  }

  setDomainFormDto = (group: GroupOfPagesFormDto) => {
    this.form.controls.url.setValue(this.getDomainFromUrl(group.pages!.at(0)!.url!));
  }
  getDomainFromUrl = (url: string): string => {
    const urlObject = new URL(url);
    return urlObject.hostname;
  }

  getPathFromUrl = (url: string): string => {
    const urlObject = new URL(url);
    return urlObject.pathname + urlObject.search + urlObject.hash;
  }

  ajouterCollaborateur(email: string[]) {
    const suiviId = +this.activatedRoute.parent!.snapshot.params['id'];
    this.tableauxDeSuiviService.ajouterUnCollaborateurASuiviSite(suiviId, email)
      .subscribe(value => window.location.reload());
  }

  getSuiviName(): string | null {
    return this.activatedRoute.snapshot.fragment;
  }

  onSubmit(): void {
    this.form.markAllAsTouched();

    if (this.form.valid && this.form.value.groupOfPages) {
      const suiviId = this.activatedRoute.parent!.snapshot.params['id'];
      this.websiteSettingsRessourceService.suivisDeSiteSettingsSuiviIdPost(suiviId, this.form.value.groupOfPages as Array<GroupOfPagesFormDto>)
        .subscribe(res => {
          this.router.navigate([`/tableaux-de-suivi/detail/${suiviId}`]);
        });
    }
  }

}