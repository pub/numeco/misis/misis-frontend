import {Component, inject, input} from '@angular/core';
import {FormArray, FormControl, FormGroup, ReactiveFormsModule} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {GroupOfPagesFormDto} from '../../../../../../rest/model/group-of-pages-form-dto';
import {WebsiteSettingsRessourceService} from '../../../../../../rest/api/website-settings-ressource.service';
import {GroupesPagesComponent} from "../../../formulaire-creation-suivi/groupes-pages/groupes-pages.component";

@Component({
    selector: 'misis-modifier-les-groupes',
    standalone: true,
    imports: [
        GroupesPagesComponent,
        ReactiveFormsModule
    ],
    template: `
        <form [formGroup]="form()">
            <misis-groupes-pages [suiviDeSiteFormGroup]="form()"/>
            <div class="fr-grid-row fr-grid-row--right fr-my-5w">
                <button class="fr-btn" type="submit" (click)="onSubmit()">
                    Enregistrer
                </button>
            </div>
        </form>
    `,
    styles: ``
})
export class ModifierLesGroupesComponent {
    private activatedRoute = inject(ActivatedRoute);
    private websiteSettingsRessourceService = inject(WebsiteSettingsRessourceService);
    private router = inject(Router);

    form = input.required<
        FormGroup<{
            groupOfPages: FormArray<FormGroup<{
                id: FormControl<number | null>;
                name: FormControl<string | null>;
                periodiciteDuSuivi: FormControl<string | null>;
                methodeDeCreationDeGroupe: FormControl<any>;
                pages: FormArray<any>;
            }>>;
        }>
    >();

    onSubmit(): void {
        this.form().markAllAsTouched();

        if (this.form().valid && this.form().value.groupOfPages) {
            const suiviId = this.activatedRoute.parent!.snapshot.params['id'];
            this.websiteSettingsRessourceService.suivisDeSiteSettingsSuiviIdPost(suiviId, this.form().value.groupOfPages as Array<GroupOfPagesFormDto>)
                .subscribe(res => {
                    this.router.navigate([`/tableaux-de-suivi/detail/${suiviId}`]);
                });
        }
    }

}
