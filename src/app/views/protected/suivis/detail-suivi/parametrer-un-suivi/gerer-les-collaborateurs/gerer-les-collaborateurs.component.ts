import {Component, input, output} from '@angular/core';
import {FormControl, FormsModule, ReactiveFormsModule, Validators} from "@angular/forms";
import {InputErrorComponent} from "../../../../../../shared/components/input-error/input-error.component";
import {InputTextComponent} from "../../../../../../shared/components/input-text/input-text.component";
import {WebsiteUserDto} from "../../../../../../rest/model/website-user-dto";

@Component({
    selector: 'misis-gerer-les-collaborateurs',
    standalone: true,
    imports: [
        FormsModule,
        ReactiveFormsModule,
        InputErrorComponent,
        InputTextComponent
    ],
    template: `
      <h2 class="fr-mb-3w">Gestion des collaborateurs</h2>
      <h3 id="fr-modal-add-user-suivi" class="fr-modal__title">Ajouter un collaborateur</h3>
      <form
        (ngSubmit)="onSubmit()">
        <!-- E-MAIL -->
        <div class="fr-grid-row fr-input-group fr-mt-3v"
             [class.fr-input-group--error]="emailCtrl.invalid && (emailCtrl.touched || emailCtrl.dirty)">
          <misis-input-text class="fr-col-sm-12"
                            label="E-mail"
                            hint="Adresse e-mail professionnelle au format : nom@domaine.fr"
                            [formControl]="emailCtrl"
                            required>
            @if (emailCtrl.errors?.['required'] && (emailCtrl.touched || emailCtrl.dirty)) {
              <misis-input-error/>
            }
            @if (emailCtrl.errors?.['email'] && (emailCtrl.touched || emailCtrl.dirty)) {
              <misis-input-error message="Vous devez saisir une adresse mail professionnelle."/>
            }
          </misis-input-text>
        </div>
        <button class="fr-btn fr-icon-user-add-line fr-btn--icon-left" type="submit" title="Ajouter un collaborateur">Ajouter un collaborateur</button>
      </form>

      <div class="fr-table fr-mt-3w" id="misis-collaborateurs-liste">
        <div class="fr-table__wrapper">
          <div class="fr-table__container">
            <div class="fr-table__content">
              <table id="misis-collaborateurs">
                <caption>
                  Liste des collaborateurs
                </caption>
                <thead>
                <tr>
                  <th scope="col">
                    Utilisateur
                  </th>
                  <th scope="col">
                    E-mail
                  </th>
                  <th class="fr-cell--fixed" role="columnheader">
                    <span class="fr-sr-only fr-cell--right">Actions</span>
                  </th>
                </tr>
                </thead>
                <tbody>
                  @for (user of users(); track user.name) {
                    <tr>
                      <td>
                        @if (user.admin) {
                          <span class="fr-icon-user-star-line" aria-hidden="true"></span>
                        }
                        {{ user.name }}
                      </td>
                      <td>
                        {{ user.email }}
                      </td>
                      <td class="fr-cell--right" scope="row">
                        @if (!user.admin) {
                          <button class="fr-btn fr-btn--tertiary fr-btn--sm fr-icon-delete-fill" title="Supprimer le collaborateur du suivi" (click)="delete(user)"></button>
                        }
                      </td>
                    </tr>
                  }
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <p class="fr-fi-warning-fill fr-text-default--warning">Tout utilisateur ajouté à un suivi dispose des mêmes droits que son créateur</p>
      </div>
    `,
    styles: `
        table {
            display: table;
            width: 100%;
        }
        .fr-cell--right {
            text-align: right;
        }
    `,
    host: {
        'class': 'fr-test'
    }
})
export class GererLesCollaborateursComponent {
  onAdd = output<string[]>();
  emailCtrl = new FormControl('', {validators: [Validators.email], nonNullable: true});
  users = input.required<Array<WebsiteUserDto>>();

  onSubmit() {
    this.emailCtrl.markAsTouched();
    if (this.emailCtrl.valid) {
      const usersEmail: string[] = this.users()
        .map(value => value.email!);
      usersEmail.push(this.emailCtrl.value);

      this.onAdd.emit(usersEmail);
      this.emailCtrl.reset();
    }
  }

  delete(user: WebsiteUserDto) {
    const usersEmail: string[] = this.users()
      .filter(value => value.id != user.id)
      .map(value => value.email!);
    this.onAdd.emit(usersEmail);
  }
}
