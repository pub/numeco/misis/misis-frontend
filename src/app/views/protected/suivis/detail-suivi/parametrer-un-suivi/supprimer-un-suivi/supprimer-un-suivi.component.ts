import {Component, inject} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {WebsiteSettingsRessourceService} from "../../../../../../rest/api/website-settings-ressource.service";

@Component({
  selector: 'misis-supprimer-un-suivi',
  standalone: true,
  imports: [],
  template: `
    <h2>Suppression du suivi</h2>
    <div class="fr-alert fr-alert--warning fr-mb-4v">
      <h3 class="fr-alert__title">Attention : cette action est irréversible</h3>
      <p>La suppression d'un suivi de site est définitive</p>
    </div>
    <button class="fr-btn fr-icon-delete-bin-fill fr-btn--icon-left" data-fr-opened="false" aria-controls="delete-suivi" title="Confirmer la suppression du suivi de site">
      Supprimer le suivi de site
    </button>

    <dialog aria-labelledby="fr-modal-delete-suivi" role="dialog" id="delete-suivi" class="fr-modal">
      <div class="fr-container fr-container--fluid fr-container-md">
        <div class="fr-grid-row fr-grid-row--center">
          <div class="fr-col-12 fr-col-md-8">
            <div class="fr-modal__body">
              <div class="fr-modal__header">
                <button class="fr-btn--close fr-btn" title="Fermer la fenêtre modale" aria-controls="delete-suivi">Fermer</button>
              </div>
              <div class="fr-modal__content">
                <h1 id="fr-modal-delete-suivi" class="fr-modal__title">
                  <span class="fr-icon-delete-bin-fill fr-icon--lg"></span> Confirmer la suppression
                </h1>
                <p>Cette action supprimera le suivi de site définitivement et il ne sera pas possible de récupérer les données associées.</p>

                <ul class="fr-btns-group fr-btns-group--icon-left">
                  <li>
                    <button class="fr-btn fr-icon-delete-bin-fill" title="Je confirme la suppression" aria-controls="delete-suivi" target="_self" (click)="confirmation()">
                      Je confirme la suppression définitive !
                    </button>
                  </li>
                  <li>
                    <button class="fr-btn fr-btn--secondary fr-icon-arrow-left-fill" aria-controls="delete-suivi" target="_self">
                      Annuler et revenir en arrière
                    </button>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </dialog>
  `,
  styles: ``
})
export class SupprimerUnSuiviComponent {

  private activatedRoute = inject(ActivatedRoute);
  private websiteSettingsRessourceService = inject(WebsiteSettingsRessourceService);
  private router = inject(Router);

  confirmation = () => {
    const suiviId = this.activatedRoute.parent!.snapshot.params['id'];
    this.websiteSettingsRessourceService.suivisDeSiteSettingsSuiviIdDelete(suiviId).subscribe(value => {
      this.router.navigate(["/tableaux-de-suivi"]);
    })
  }
}
