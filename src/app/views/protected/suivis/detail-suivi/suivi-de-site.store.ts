import {Injectable, signal} from '@angular/core';
import {SuiviSite} from "../formulaire-creation-suivi/model/suivi-site.model";

@Injectable({
    providedIn: 'root'
})
export class SuiviDeSiteStore {

    #suiviDeSite = signal<SuiviSite | null>(null);
    suiviDeSite = this.#suiviDeSite.asReadonly();
    websiteId: number;

    setWebsiteId(websiteId: number) {
        this.websiteId = websiteId
    }
}
