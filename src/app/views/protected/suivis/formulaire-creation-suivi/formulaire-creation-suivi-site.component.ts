import {ChangeDetectionStrategy, Component, inject} from '@angular/core';
import {FormBuilder, ReactiveFormsModule} from "@angular/forms";
import {Router} from "@angular/router";
import {TableauxDeSuiviService} from "../tableaux-de-suivi.service";
import {buildSuiviDeSiteForm, SuiviSite} from "./model/suivi-site.model";
import {buildGroupePagesFormGroupWith} from "./model/group-pages.model";
import {takeUntilDestroyed} from "@angular/core/rxjs-interop";
import {InputTextComponent} from "../../../../shared/components/input-text/input-text.component";
import {InputErrorComponent} from "../../../../shared/components/input-error/input-error.component";
import {GroupesPagesComponent} from "./groupes-pages/groupes-pages.component";


@Component({
    selector: 'misis-formulaire-creation-suivi-site',
    standalone: true,
  imports: [
    ReactiveFormsModule,
    InputTextComponent,
    InputErrorComponent,
    GroupesPagesComponent
  ],
    template: `
      <div class="fr-container fr-mt-3w">
        <h2 class="fr-mb-2w">Paramétrer un suivi de site</h2>

        <form [formGroup]="form" style="flex-grow: 1">
          <div class="fr-grid-row fr-grid-row--gutters">
            <!-- Nouveau site -->
            <misis-input-text class="fr-col-12 fr-col-md-6"
                              label="Nom du suivi"
                              hint="Nom d'affichage du suivi de site"
                              formControlName="name"
                              required>
              @if (form.controls.name.touched) {
                @if (form.controls.name.errors?.['required']) {
                  <misis-input-error/>
                }
                @if (form.controls.name.errors?.['pattern']) {
                  <misis-input-error message="Ce champ ne doit pas être vide."/>
                }
              }
            </misis-input-text>
            <!-- URL -->
            <misis-input-text class="fr-col-12 fr-col-md-6"
                              label="Nom de domaine"
                              hint="La racine de votre site, par exemple https://votre-site.fr"
                              formControlName="url"
                              addon="https://"
                              tooltip="Seule la racine du site sera prise en compte ; le cas échéant, un groupe sera automatiquement créé avec la page saisie"
                              required>
              @if (form.controls.url.errors?.['required'] && form.controls.url.touched) {
                <misis-input-error/>
              }
              @if (form.controls.url.errors?.['pattern'] && form.controls.url.touched) {
                <misis-input-error message="Le format de l'url est incorrect."/>
              }
            </misis-input-text>
          </div>


          <!--  GROUPE DE SITES  -->
          <misis-groupes-pages [suiviDeSiteFormGroup]="form"/>
        </form>

        <!--  BOUTON DE VALIDATION  -->
        <div class="fr-grid-row fr-grid-row--right fr-my-5w">
          <button class="fr-btn" type="submit" (click)="onSubmit()" title="Enregistrer le suivi">
            Enregistrer le suivi
          </button>
        </div>
      </div>
    `,
    host: {
        style: 'display: flex; flex-direction: column; height: 100%;',
    },
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export default class FormulaireCreationSuiviSiteComponent {

    /** Dependencies */
    private readonly fb = inject(FormBuilder);
    private readonly tableauxDeSuiviService = inject(TableauxDeSuiviService);
    private readonly router = inject(Router);

    form = buildSuiviDeSiteForm();

    constructor() {
        this.managePagesOnUrlChanges();
    }

    onSubmit(): void {
        this.form.markAllAsTouched();

        if (this.form.valid) {
            this.tableauxDeSuiviService.create(this.form.value as SuiviSite).subscribe((res) => {
                this.router.navigate(['/tableaux-de-suivi']);
            });
        }
    }

    managePagesOnUrlChanges(): void {
        this.form.controls.url.valueChanges
            .pipe(takeUntilDestroyed())
            .subscribe(value => {
                if (value) {
                    const capture = value.match(/^(?:https?:\/\/)?([^\/]+)(\/.*)?/i)
                    if (capture && capture?.[1] !== value) {
                        this.form.controls.url.setValue(capture[1]);
                    }
                    if (capture?.[2]) {
                        const group = this.form.controls.groupOfPages;
                        if (group.length === 0) {
                            group.push(buildGroupePagesFormGroupWith(capture?.[2]));
                        }
                    }
                }
            });
    }
}

