import {Component, inject} from '@angular/core';
import {CreationStep1Component} from "../creation-step/creation-step1.component";
import {CreationStep2Component} from "../creation-step/creation-step2.component";
import {buildPageFormGroupWith, creationForm, creationStepTitles, GroupCreationForm, managePagesOnUrlChanges} from "../creation-step/creation-step-configuration";
import {StepperComponent} from "../../../../shared/components/stepper/stepper.component";
import {takeUntilDestroyed} from "@angular/core/rxjs-interop";
import {FormArray, FormControl} from "@angular/forms";
import {SuiviSite} from "./model/suivi-site.model";
import {TableauxDeSuiviService} from "../tableaux-de-suivi.service";
import {Router} from "@angular/router";
import { debounce, debounceTime } from 'rxjs';


@Component({
  selector: 'div[misis-creation-suivi-site][class="fr-container"]',
  standalone: true,
  imports: [
    CreationStep1Component,
    CreationStep2Component,
    StepperComponent,
  ],
  template: `
    <h1>
      Paramétrer un suivi de site
    </h1>
    <misis-stepper [currentStep]="currentStep" [stepTitles]="creationStepTitles"></misis-stepper>
    @switch (currentStep) {
      @case (1) {
        <div class="fr-container" misis-creation-step1 [form]="form.controls.step1" (processToStep)="goTo($event)"></div>
      }
      @case (2) {
        <div class="fr-container" misis-creation-step2 [form]="form.controls.step2" (processToStep)="goTo($event)"></div>
      }
    }
  `,
  host: {},
})
export default class CreationSuiviSiteComponent {
  private readonly tableauxDeSuiviService = inject(TableauxDeSuiviService);
  private readonly router = inject(Router);

  currentStep: number = 1;
  form = creationForm();

  constructor() {
    managePagesOnUrlChanges(this.form.controls.step2);
  }

  goTo = (stepNumber: number) => {
    if (stepNumber) {
      if (stepNumber === creationStepTitles.length+1) {
        this.endStepper();
      }else {
        this.currentStep = stepNumber;
      }
    } else {
      this.currentStep = 1;
    }
  }

  endStepper = () => {
    if (this.form.valid) {
      const suiviSiteForm = {
        ...this.form.controls.step1.value,
        ...this.form.controls.step2.value,
      }
      this.tableauxDeSuiviService.create(suiviSiteForm as SuiviSite)
      .pipe(
        debounceTime(500),
      )
      .subscribe((res) => {
        this.form.disable();
        this.form.reset();
        this.router.navigate(['/tableaux-de-suivi']);
        this.form.enable();
      });
    }
  }
  protected readonly creationStepTitles = creationStepTitles;
}

