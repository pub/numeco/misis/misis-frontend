import {AbstractControl, FormArray, FormControl, FormGroup, Validators} from "@angular/forms";
import {MethodeDeCreationDeGroupe, PeriodiciteDuSuivi} from "./suivi-site.enums";
import {Constants} from "../../../../../constants";

export interface GroupPages {
    id?: number;
    name: string;
    periodiciteDuSuivi: PeriodiciteDuSuivi;
    methodeDeCreationDeGroupe: MethodeDeCreationDeGroupe;
    pages: Page[];
}

export interface Page {
    id: number;
    url: string;
    dataDecodedInBytes: number;
    dataTransferedInBytes: number;
    domElementsCount: number;
    httpRequestsCount: number;
    ressources: Ressource[];
}

export interface Ressource {
    id: number;
    filename: string;
    url: string;
    sizeInBytes: number;
    type: RessourceType;
}

export type RessourceType = 'IMAGE' | 'PDF';

export function buildGroupePagesFormGroup() {
    return new FormGroup({
        id:new FormControl<number | null>(null),
        name: new FormControl('', Validators.pattern(Constants.NOT_BLANK)),
        periodiciteDuSuivi: new FormControl(''),
        methodeDeCreationDeGroupe: new FormControl(''),
        pages: new FormArray([buildPagesFormGroup()]),
    });
}

export type GroupePagesFormGroup = ReturnType<typeof buildGroupePagesFormGroup>;


export function buildPagesFormGroup() {
    return new FormGroup({
        id: new FormControl<number | null>(null),
        url: new FormControl('', [
          Validators.required,
          (control: AbstractControl): { [key: string]: any } | null => {
            const originalUrl = control.value;
            const capture = originalUrl.match(/^(?:https?:\/\/)?([^\/]+)(\/.*)?/i)
            if (capture?.[2] && capture[1] !== control.value) {
                control.setValue(capture[2]);
            }
            return null;
          },
          Validators.pattern(Constants.PATH_PATTERN),
        ]),
    });
}

export function buildGroupePagesFormGroupWith(path : string) : FormGroup {
  return new FormGroup({
    id:new FormControl<number | null>(null),
    name: new FormControl('Groupe de pages n°1', Validators.pattern(Constants.NOT_BLANK)),
    periodiciteDuSuivi: new FormControl('QUOTIDIEN'),
    methodeDeCreationDeGroupe: new FormControl('MANUELLE'),
    pages: new FormArray([buildPageFormGroupWith(path)]),
  });
}

export function buildPageFormGroupWith(path: string) {
  return new FormGroup({
    id: new FormControl<number | null>(null),
    url: new FormControl<string | null>(path, [Validators.required, Validators.pattern(Constants.PATH_PATTERN)]),
  });
}
