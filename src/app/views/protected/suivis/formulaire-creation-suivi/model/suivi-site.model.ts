import {GroupePagesFormGroup, GroupPages} from "./group-pages.model";
import {FormArray, FormControl, FormGroup, Validators} from "@angular/forms";
import {Constants} from "../../../../../constants";

export interface SuiviSite {
    id?: number;
    name: string;
    url?: string;
    groupOfPages: GroupPages[];
}

export function buildSuiviDeSiteForm() {
    return new FormGroup({
        name: new FormControl('', [Validators.required,Validators.pattern(Constants.NOT_BLANK)]),
        url: new FormControl('', [Validators.required,Validators.pattern(Constants.DOMAIN_PATTERN)]),
        groupOfPages: new FormArray<GroupePagesFormGroup>([]),
    });
}
