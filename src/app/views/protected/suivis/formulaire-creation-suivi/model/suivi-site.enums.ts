export enum PeriodiciteDuSuivi {
    QUOTIDIEN = 'Quotidien',
    HEBDOMADAIRE = 'Hebdomadaire',
    MENSUEL = 'Mensuel',
}

export enum MethodeDeCreationDeGroupe {
    MANUELLE = 'Manuelle',
}
