import {AfterViewInit, Component, input} from '@angular/core';
import {FormArray, FormControl, FormGroup, ReactiveFormsModule} from "@angular/forms";
import {buildPagesFormGroup} from "../../../model/group-pages.model";
import {InputErrorComponent} from "../../../../../../../shared/components/input-error/input-error.component";
import {InputTextComponent} from "../../../../../../../shared/components/input-text/input-text.component";

let uniqueId = 0;

@Component({
    selector: 'misis-formulaire-groupe-pages',
    standalone: true,
    imports: [
        ReactiveFormsModule,
        InputErrorComponent,
        InputTextComponent,
    ],
    templateUrl: `./formulaire-groupe-pages.component.html`,
    styles: `
    `
})
export class FormulaireGroupePagesComponent implements AfterViewInit{

    /** Inputs */
    readonly form = input.required<FormGroup>();
    readonly domain = input<string>();

    id = uniqueId++;

    ngAfterViewInit() {
      this.pagesFormArray?.valueChanges.subscribe()
    }

    addPage() {
        this.pagesFormArray.push(buildPagesFormGroup());
    }

    removePage(index: number) {
        if (this.pagesFormArray.length === 1) return;
        this.pagesFormArray.removeAt(index);
    }

    get pagesFormArray(): FormArray<FormGroup> {
        return this.form().get('pages') as FormArray;
    }

    get methodeCreationGroupeCtrl() {
        return this.form().get('methodeDeCreationDeGroupe') as FormControl;
    }

}
