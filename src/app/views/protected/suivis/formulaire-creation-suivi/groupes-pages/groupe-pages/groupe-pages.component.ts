import {Component, input, output} from '@angular/core';
import {FormControl, FormGroup, ReactiveFormsModule} from "@angular/forms";
import {FormulaireGroupePagesComponent} from "./formulaire-groupe-pages/formulaire-groupe-pages.component";


@Component({
    selector: 'misis-groupe-pages',
    standalone: true,
    imports: [
        ReactiveFormsModule,
        FormulaireGroupePagesComponent
    ],
    template: `
        <section class="groupe-pages">
            <section class="fr-accordion">
                <h3 class="fr-accordion__title">
                    <button class="fr-accordion__btn" aria-expanded="false"
                            [attr.aria-controls]="'group-pages-' + index()" title="Afficher les options du groupe">
                        {{ nomGroupeCtrl.value ?? 'Groupe de page n°' + (index() + 1) }}
                    </button>
                </h3>
                <div class="fr-collapse" [id]="'group-pages-' + index()">
                    <misis-formulaire-groupe-pages [domain]="domain()" [form]="groupeDePagesFormGroup()"/>
                </div>
            </section>
            <button class="fr-btn fr-btn--tertiary fr-btn--sm fr-icon-delete-fill" title="Supprimer le groupe de pages" (click)="onRemove.emit(index())"></button>
        </section>
    `,
    styles: `
      .groupe-pages {
        display: flex;
        align-items: baseline;
        justify-content: space-between;
        gap: 1rem;

        .fr-accordion {
          flex: 1;
        }
      }
    `
})
export class GroupePagesComponent {

    /** Inputs */
    readonly groupeDePagesFormGroup = input.required<FormGroup>();
    readonly index = input.required<number>();
    readonly domain = input<string>();
    /** Outputs */
    readonly onRemove = output<number>();


    get nomGroupeCtrl(): FormControl {
        return this.groupeDePagesFormGroup().get('name') as FormControl;
    }

}
