import {Component, input} from '@angular/core';
import {FormArray, FormGroup} from "@angular/forms";
import {GroupePagesComponent} from "./groupe-pages/groupe-pages.component";
import {buildGroupePagesFormGroup, GroupePagesFormGroup} from "../model/group-pages.model";

@Component({
    selector: 'misis-groupes-pages',
    standalone: true,
  imports: [
    GroupePagesComponent
  ],
    template: `
        <div class="flex-between fr-my-2w fr-grid-row">
            <div class="fr-col-12 fr-col-md-6">
                <h3 class="fr-mb-1v">Groupe de pages</h3>
            </div>
            <button class="fr-btn" (click)="addGroupe()" title="Ajouter un groupe d'URL à analyser">Ajouter un groupe</button>
        </div>

        @for (groupCtrl of groupesDePagesArray.controls; track $index) {
            <misis-groupe-pages [domain]="suiviDeSiteFormGroup().get('url')?.value" [groupeDePagesFormGroup]="groupCtrl"
                                [index]="$index" (onRemove)="removeGroup($event)"/>
        } @empty {
            <div class="fr-alert fr-alert--info fr-alert--sm">
                <p>Si aucun groupe n'est ajouté, un groupe par défaut sera créé à partir de l'URL saisie.</p>
            </div>
        }
    `,
    styles: ``
})
export class GroupesPagesComponent {

    /** Inputs */
    readonly suiviDeSiteFormGroup = input.required<FormGroup>();

    addGroupe() {
        this.groupesDePagesArray.push(buildGroupePagesFormGroup());
    }

    removeGroup(index: number) {
        this.groupesDePagesArray.removeAt(index);
    }

    get groupesDePagesArray() {
        return this.suiviDeSiteFormGroup().get('groupOfPages') as FormArray<GroupePagesFormGroup>;
    }

}
