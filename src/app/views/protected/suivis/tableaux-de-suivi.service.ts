import { HttpClient } from "@angular/common/http";
import { inject, Injectable } from '@angular/core';
import { environment } from "../../../../environments/environment";
import { SuiviDeSiteStore } from "./detail-suivi/suivi-de-site.store";
import { SuiviSite } from "./formulaire-creation-suivi/model/suivi-site.model";

@Injectable({
    providedIn: 'root'
})
export class TableauxDeSuiviService {

    SERVEUR_API_URL = `${environment.BASE_API}/suivis-de-site`;
    http = inject(HttpClient);
    suiviDeSiteStore = inject(SuiviDeSiteStore);

    create(suiviDeSite: SuiviSite) {
        return this.http.post(`${this.SERVEUR_API_URL}`, suiviDeSite, { withCredentials: true });
    }

    ajouterUnCollaborateurASuiviSite(idSuivi: number, emailCollaborateur: string[]) {
        return this.http.post<void>(`${this.SERVEUR_API_URL}/settings`, {
            suivisId: [idSuivi],
            usersEmail: emailCollaborateur
        }, { withCredentials: true });
    }
}
