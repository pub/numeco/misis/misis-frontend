import {Component, inject} from '@angular/core';
import {NotificationService} from "./notification.service";
import {AsyncPipe} from "@angular/common";
import {RouterLink} from "@angular/router";
import {TimeAgoPipe} from "../../../shared/pipes/time-ago-pipe";
import {Notification} from "./notification";

@Component({
  selector: 'misis-notification',
  standalone: true,
  imports: [
    AsyncPipe,
    RouterLink,
    TimeAgoPipe
  ],
  template: `
    <div class="fr-container fr-mt-3w">
      @if (notifications | async; as notif) {

        @if (notif.length) {
          <h2 class="fr-mb-2w">
            Mes événements
          </h2>
          <div class="fr-grid-row fr-mb-1w">
            <div class="fr-mb-1w fr-col-10 fr-col-offset-1">
              <button type="button" class="fr-btn float-right" (click)="markAllAsRead(notif)">
                Tout ignorer
              </button>
            </div>

          @for (value of notif; track value.id) {
              <div class="fr-col-10 fr-col-offset-1">
                <div class="fr-card fr-card--no-arrow fr-enlarge-link fr-card--sm" (click)="markAsRead(value)">
                  <div class="fr-card__body">
                    <div class="fr-card__content">
                      <div class="fr-card__desc">
                        <p>
                          {{ value.message }}
                        </p>
                      </div>
                      <div class="fr-card__end">
                        <button>
                          Marqué comme lu
                        </button>
                      </div>
                    </div>
                  </div>
                  <div class="fr-card__header fr-card--sm">
                    <ul class="fr-badges-group">
                      <li>
                        <p class="fr-badge"
                           [class.fr-badge--green-emeraude]="value.visible"
                           [class.fr-badge--new]="value.visible"
                           [class.fr-badge--beige-gris-galet]="!value.visible"
                        >
                          {{ value.createdAt | timeAgo }}
                        </p>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
          }
          </div>
        } @else {
          <h2 class="fr-mb-2w">
            Aucune notification
          </h2>
          <a class="fr-link fr-icon-arrow-left-line fr-link--icon-left" routerLink="/accueil">
            Revenir à l'accueil
          </a>
        }
      } @else {
        <a class="fr-link fr-icon-arrow-left-line fr-link--icon-left" routerLink="/accueil">
          Revenir à l'accueil
        </a>
      }
    </div>
  `,
  styles: `
    .float-right {
      float: right;
    }
  `
})
export default class NotificationComponent {

  notificationService = inject(NotificationService);

  notifications = this.notificationService.notifications$;

  markAsRead = (notification: Notification) => this.notificationService.markAsRead(notification).subscribe()

  markAllAsRead = (notifications: Array<Notification>) => this.notificationService.markAllAsRead(notifications).subscribe()

}
