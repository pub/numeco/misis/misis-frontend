import { HttpClient } from "@angular/common/http";
import { inject, Injectable } from '@angular/core';
import { shareReplay } from "rxjs";
import { environment } from "../../../../environments/environment";
import { Notification } from "./notification";

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  private readonly http = inject(HttpClient);
  private readonly url = `${environment.BASE_API}/notifications`;
  notifications$ = new Promise<Array<Notification>>(resolve => {
    setTimeout(() => {
      this.http.get<Array<Notification>>(this.url, { withCredentials: true })
      .pipe(
        shareReplay({ bufferSize: 1, refCount: true })
      )
      .subscribe(notifications => resolve(notifications));
    }, 1000);
  });

  public markAsRead = (notification: Notification) => {
    notification.visible = false;
    return this.http.patch(this.url, [notification], { withCredentials: true })
  }

  public markAllAsRead = (notifications: Array<Notification>) => {
    notifications.forEach(notification => notification.visible = false);
    return this.http.patch(this.url, notifications, { withCredentials: true })
  }

}
