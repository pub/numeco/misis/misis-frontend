export interface Notification {
  id: number;
  message: string;
  createdAt: Date;
  visible: boolean;
}
