import { Component } from '@angular/core';

@Component({
  selector: 'div [class="fr-container"] [misis-mentions-legales]',
  standalone: true,
  imports: [
  ],
  template: `
<h1>Mentions légales</h1>
<h2>Éditeur</h2>
<p>Le site est édité par la Direction du Numérique (DNUM) des Ministères Aménagement du Territoire Transition Écologique&nbsp;:
</p>
<p>Grand Arche paroi Sud Parvis de La Défense<br>
   92800 Puteaux<br>
    France<br>
    <a href="mailto:numerique-ecologie@developpement-durable.gouv.fr">numerique-ecologie{{'@'}}developpement-durable.gouv.fr</a>.
</p>
<h2>Directeur de la publication</h2>
<p>Arnaud Beaufort, directeur de la Direction du Numérique.</p>
<p>La conception éditoriale, le suivi et les mises à jour du site internet
    sont assurés par les services de la DNUM.
</p>
<h2>Hébergement</h2>
<p>SECRETARIAT GENERAL DU MINISTERE DE LA TRANSITION ECOLOGIQUE ET SOLIDAIRE (SG)<br>
LA GRANDE ARCHE PAROI SUD 92800 PUTEAUX<br>
SIRET : 13001954000025
Administration publique générale (84.11Z)
Téléphone : 01 40 81 21 22
</p>
<h2>Remontée d'anomalie de sécurité</h2>
<p>Dans le cadre de notre politique de sécurité des systèmes d'information (PSSI) et conformément aux recommandations de l'Agence Nationale de la Sécurité des Systèmes d'Information (ANSSI), nous mettons à votre disposition ce formulaire de déclaration d'incident ou d'anomalie de sécurité.</p>
<p>Pour effectuer une remontée d'anomalie de sécurité, merci de remplir le formulaire suivant : <a href="https://framaforms.org/remontee-de-probleme-de-securite-sur-le-produit-misis-1733130339">https://framaforms.org/remontee-de-probleme-de-securite-sur-le-produit-misis-1733130339</a>. Aucun autre type de demande ne pourra être pris en compte par ce formulaire.</p>
<h2>Protection des contenus</h2>
<p>Sauf mention expresse contraire, tous les contenus protégeables par le droit de la
    Propriété intellectuelle ne peuvent être reproduits ou réutilisés sans l'autorisation
    expresse de la Direction du Numérique.
</p>
<h2>Demande d'autorisation</h2>
<p>Les demandes d'autorisation de reproduction d'un contenu doivent au préalable être
    adressées à la DNUM, en écrivant à l'adresse suivante : <a
        href="mailto:numerique-ecologie@developpement-durable.gouv.fr">numerique-ecologie{{'@'}}developpement-durable.gouv.fr</a>.
</p>
<p>La demande devra préciser le contenu visé ainsi que le contexte d'utilisation prévu
    (supports concernés, période, destinataires, etc.).</p>
  `,
  styles: ``
})
export default class MentionsLegalesComponent {

}
