import {Component} from '@angular/core';

@Component({
  selector: 'div [class="fr-container"] [misis-donnees-personnelles]',
  standalone: true,
  imports: [
  ],
  template: `
  <div class="fr-col-12 fr-col-md-8">
      <h1>Données personnelles et cookies</h1>
      <h2>Données personnelles</h2>
      <p>Ce site ne prélève aucune donnée à caractère personnel.</p>
      <h2>Gestion des cookies</h2>
      <p>La CNIL définit les cookies comme « un petit fichier stocké par un serveur dans le terminal (ordinateur, téléphone, etc.) d’un utilisateur et associé à un domaine web (c'est-à-dire dans la majorité des cas à l’ensemble des pages d’un même site web). Ce fichier est automatiquement renvoyé lors de contacts ultérieurs avec le même domaine ».</p>
      <p>En savoir plus sur les cookies utilisés :</p>
      <p>MISIS ne gère que les outils liés à l'authentification des utilisateurs.</p>
      <h2>En savoir plus sur la réglementation</h2>
      <p>
        <a class="fr-link fr-icon-arrow-right-line fr-link--icon-left" rel="noopener external" target="_blank" title="Le règlement général sur la protection des données (RGPD) - nouvelle fenêtre" href="https://www.cnil.fr/fr/reglement-europeen-protection-donnees">Le règlement général sur la protection des données (RGPD)</a>
      </p>
      <p>
        <a class="fr-link fr-icon-arrow-right-line fr-link--icon-left" rel="noopener external" target="_blank" title="La loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés - nouvelle fenêtre" href="https://www.legifrance.gouv.fr/loda/id/JORFTEXT000000886460/">La loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés</a>
      </p>
      <hr />
  </div>
  `,
  styles: ``
})
export default class DonneesPersonnellesComponent {

}
