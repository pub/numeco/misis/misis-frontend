import {Component, inject, signal} from '@angular/core';
import {Route, Router, RouterLink} from "@angular/router";
import {WebsiteResourceService} from "../../../rest/api/website-resource.service";
import {WebsiteSummaryDto} from "../../../rest/model/website-summary-dto";
import { AccountService } from '../../../core/auth/account.service';

@Component({
  selector: 'div [class="fr-container"] [misis-plan-du-site]',
  standalone: true,
  imports: [
    RouterLink
  ],
  template: `
    <h1>Plan du site</h1>
    <ul>
      @for (route of sitemap(); track route.path) {
        <li>
          <a [routerLink]="route.path">{{ route.title }}</a>
          @if (route.children.length > 0) {
            <ul>
              @for (child of route.children; track child.path) {
                <li>
                  <a [routerLink]="child.path">{{ child.title }}</a>
                  @if (child.children.length > 0) {
                    <ul>
                      @for (child2 of child.children; track child2.path) {
                        <li>
                          <a [routerLink]="child2.path">{{ child2.title }}</a>
                          @if (child2.children.length > 0) {
                            <ul>
                              @for (child3 of child2.children; track child3.path) {
                                <li>
                                  <a [routerLink]="child3.path">{{ child3.title }}</a>
                                  @if (child3.children.length > 0) {
                                    <ul>
                                      @for (child4 of child3.children; track child4.path) {
                                        <li>
                                          <a [routerLink]="child4.path">{{ child4.title }}</a>
                                          @if (child4.children.length > 0) {
                                            <ul>
                                              @for (child5 of child4.children; track child5.path) {
                                                <li>
                                                  <a [routerLink]="child5.path">{{ child5.title }}</a>
                                                </li>
                                              }
                                            </ul>
                                          }
                                        </li>
                                      }
                                    </ul>
                                  }
                                </li>
                              }
                            </ul>
                          }
                        </li>
                      }
                    </ul>
                  }
                </li>
              }
            </ul>
          }
        </li>
      }
    </ul>
  `,
  styles: ``
})
export default class PlanDuSiteComponent {
  accountService: AccountService = inject(AccountService)
  websiteResourceService: WebsiteResourceService = inject(WebsiteResourceService)
  websiteSummary: WebsiteSummaryDto | null;
  sitemap = signal<Sitemap[]>([]);

  routes: Route[];

  constructor(private router: Router) {
    this.routes = this.router.config;

    if (this.accountService.account().status === 'CONNECTED') {
      this.websiteResourceService.suivisDeSiteGet().subscribe(value => {
        this.websiteSummary=value[0];
        this.getRoutes(this.routes, '', this.websiteSummary);
      })
    }
    this.getRoutes(this.routes, '', this.websiteSummary);
  }

  getFullPath(route: Route, parentPath: string = '', websiteSummary: WebsiteSummaryDto | null): string | undefined {
    let fullPath = parentPath ? `/${parentPath}/${route.path}` : `/${route.path}`;

    return websiteSummary && this.accountService.account().status === 'CONNECTED'
      ? fullPath?.replace(':id', `${websiteSummary.id}`).replace(':id', `${Object.keys(websiteSummary.groups!)[0]}`)
      : fullPath;
  }

  getRoutes(routes: Route[], parentPath: string = '', websiteSummary: WebsiteSummaryDto | null): Sitemap[] {
    let sitemapRoutes: Sitemap[] = [];
    for (let route of routes) {
      if (!route.data?.['title'] || (route.canMatch && this.accountService.account().status !== 'CONNECTED')) {
        continue;
      }
      const fullPath = this.getFullPath(route, parentPath, websiteSummary);
      sitemapRoutes.push({title: route.data?.['title'], path: fullPath, children: route.children ? this.getRoutes(route.children, fullPath, websiteSummary) : []});
    }

    this.sitemap.set(sitemapRoutes)
    return sitemapRoutes;
  }

}

export interface Sitemap {
  title: string;
  path: string | undefined;
  children: Sitemap[];
}
