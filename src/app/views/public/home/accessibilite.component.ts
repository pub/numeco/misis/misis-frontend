import { Component } from '@angular/core';

@Component({
  selector: 'div [class="fr-container"] [misis-accessibilite]',
  standalone: true,
  imports: [],
  template: `
  <div class="fr-col-12 fr-col-md-8">
    <h1>Déclaration d’accessibilité</h1>
    <p>
    	Établie le
    	<span>13 février 2025</span>.
    </p>
    <p>
    	<span>Ministères,aménagement du territoire - Transition écologique</span>
    	s’engage à rendre son service accessible, conformément à
    	l’article 47 de la loi n° 2005-102 du 11 février 2005.
    </p>
    <p>
    	Cette déclaration d’accessibilité s’applique à
    	<strong>Outil de Mesure d'Impact des Sites Internet et leur Suivi (MISIS)</strong><span>
    	(<span>https://misis.developpement-durable.gouv.fr</span>)</span>.
    </p>
    <h2>État de conformité</h2>
    <p>
    	<strong>Outil de Mesure d'Impact des Sites Internet et leur Suivi (MISIS)</strong>
    	est
    	<strong><span data-printfilter="lowercase">non conforme</span></strong>
    	avec le
    	<abbr title="Référentiel général d’amélioration de l’accessibilité">RGAA</abbr>. <span>Le site n’a encore pas été audité.<br></span>
    </p>
    <h2>Amélioration et contact</h2>
    <p>
    	Si vous n’arrivez pas à accéder à un contenu ou à un service,
    	vous pouvez contacter le responsable de
    	<span>Outil de Mesure d'Impact des Sites Internet et leur Suivi (MISIS)</span>
    	pour être orienté vers une alternative accessible ou obtenir le
    	contenu sous une autre forme.
    </p>
    <ul class="basic-information feedback h-card">
    	<li>
    	Téléphone&nbsp;: <span>+33 763794940 </span>
    </li>
    	<li>
    	E-mail&nbsp;:
    	<a href="mailto:sarah.marais-laballery@developpement-durable.gouv.fr">sarah.marais-laballery&#64;developpement-durable.gouv.fr</a>
    </li>

    	<li>
    	Formulaire de contact&nbsp;:
    	<a href="https://framaforms.org/experimentez-misis-en-avant-premiere-1718953308?info=republished">Nous contacter</a>
    </li>
    	<li>
    	Adresse&nbsp;: <span>Direction du numérique- Secrétariat généralPôles activités CS 70499 13593 AIX-EN-PROVENCE</span>
    </li>
    </ul>
    <p>
    	Nous essayons de répondre dans les
    	<span>"2 jours ouvrés"</span>.
    </p>
    <h2>Voie de recours</h2>
    <p>
    	Cette procédure est à utiliser dans le cas suivant&nbsp;: vous avez
    	signalé au responsable du site internet un défaut
    	d’accessibilité qui vous empêche d’accéder à un contenu ou à un
    	des services du portail et vous n’avez pas obtenu de réponse
    	satisfaisante.
    </p>
    <p>Vous pouvez&nbsp;:</p>
    <ul>
    	<li>
    	  Écrire un message au
    	  <a href="https://formulaire.defenseurdesdroits.fr/">Défenseur des droits</a>
      </li>
      	<li>
        	Contacter
        	<a href="https://www.defenseurdesdroits.fr/saisir/delegues">le délégué du Défenseur des droits dans votre région</a>
      </li>
      <li>
      	Envoyer un courrier par la poste (gratuit, ne pas mettre de timbre)&nbsp;:<br>
      	Défenseur des droits<br>
      	Libre réponse 71120 75342 Paris CEDEX 07
      </li>
    </ul>
    <hr>
    <p>
    	Cette déclaration d’accessibilité a été créé le
    	<span>13 février 2025</span>
    	grâce au
    	<a href="https://betagouv.github.io/a11y-generateur-declaration/#create">Générateur de Déclaration d’Accessibilité</a>.
    </p>
  </div>
`,
  styles: ``
})
export default class AccessibiliteComponent {

}
