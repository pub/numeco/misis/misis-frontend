import {Component} from '@angular/core';
import {RouterLink} from "@angular/router";

@Component({
  selector: 'misis-home',
  standalone: true,
  imports: [
    RouterLink
  ],
  template: `

      <!--  FIRST SECTION  -->
      <div class="accueil-row half-bg">
          <div class="fr-container">
              <div class="fr-grid-row fr-grid-row--gutters">
                  <!-- LEFT SIDE -->
                  <article class="fr-col-md-7 fr-col-sm-12">
                      <h1 class="fr-mb-3w">MISIS</h1>
                      <p class="accueil-row--text">Analysez l'impact des sites internet au regard du principe d'écoconception, en particulier pour les sites web à contenu.</p>
                  </article>
                  <!-- RIGHT SIDE -->
                  <article class="fr-col-md-5 fr-col-sm-12 accueil-row--side">
                      <h2 class="fr-mb-3w">Tout chemin commence par un premier pas.</h2>
                      <p>Définissez le site que vous souhaitez suivre, laissez-vous guider, et observez l'impact de votre site web.</p>
                      <button class="fr-btn" routerLink="/creation-suivi-de-site" title="Ajouter un site">Ajouter un site</button>
                  </article>
              </div>
          </div>
      </div>

      <!-- SECOND SECTION -->
      <div class="fr-container fr-my-6w">
          <div class="fr-grid-row fr-grid-row--gutters">
              <!-- LEFT SIDE -->
              <article class="fr-col-lg-5 fr-col-md-12">
                  <h2 class="fr-mb-3w">Comment marche MISIS ?</h2>
                  <p class="accueil-row--text">
                  C'est simple : vous saisissez l'url du site que vous souhaitez analyser. MISIS analyse le contenu des pages de votre site et indique le nombre d'images de plus de 100 ko, le poids des documents PDF, la quantité de données transférées et décodées.
                  </p>
                  <p class="accueil-row--text">Grâce à votre espace personnel, lié à votre compte ProConnect (anciennement AgentConnect), vous pouvez suivre l'évolution des sites analysés. En effet, MISIS mesure l'évolution de l'impact dans le temps via l'historique des analyses avec un résultat graphique.</p>
              </article>
              <!-- RIGHT SIDE -->
              <article class="fr-col-lg-7 fr-col-md-12 accueil-row--side fr-pt-1w">
                  <figure class="fr-content-media" role="group" aria-label="MISIS comme outil d'écoconception">
                      <div class="fr-content-media__img">
                          <img class="fr-responsive-img fr-ratio-16x9" src="assets/img/outils.webp" alt=""/>
                      </div>
                  </figure>
              </article>
          </div>
      </div>

  `,
  styles: `

    .half-bg {
      background: linear-gradient(to right, var(--background-alt-blue-france) 60%, var(--background-default-grey) 40%);
      article {
        padding-top: 5rem;
        padding-bottom: 5rem;
      }
    }

    .accueil-row--side {
      background-color: var(--background-default-grey);
      padding: 0 3rem;

      figure {
        margin: 0;
        padding: 0;
      }
    }

    .accueil-row--text {
      font-weight: 500;
      font-size: 18px;
    }

    @media (max-width: 768px) {
      .half-bg {
        background: var(--background-alt-blue-france);

        article {
          padding-top: 2.5rem;
          padding-bottom: 2.5rem;
        }

        .accueil-row--side {
          background-color: var(--background-alt-blue-france);
          padding: 0 .5rem;
          button {
            right: 0;
          }
        }
      }

      .accueil-row--side {
        margin-bottom: 20px;
      }
    }
    `
})
export default class HomeComponent {

}
