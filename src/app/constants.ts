export class Constants {
    public static readonly URL_PATTERN = /^https?:\/\/[^\s/$.?#]+\.[^\s]+$/;
    public static readonly PATH_PATTERN = /^\/.*$/;
    public static readonly DOMAIN_PATTERN = /^[^\s/$.?#]+\.[^\s]+$/;
    public static readonly NOT_BLANK = /^(?!\s*$).+/
}
