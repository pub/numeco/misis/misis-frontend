import { HttpClient } from '@angular/common/http';
import { inject, Injectable } from '@angular/core';
import { map, Observable, tap } from "rxjs";
import { AccountService } from './account.service';
import { UserInfos } from './models/account.model';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  private readonly http = inject(HttpClient);
  private readonly accountService = inject(AccountService);

  private readonly LOGIN_API_URL = `/oauth2`;
  private readonly AUTH_STATUS_API_URL = `/oauth2/status`;
  private readonly LOGOUT_API_URL = `/oauth2/logout`;

  login(): void {
    this.accountService.setIsConnecting();
    window.location.href = `${this.LOGIN_API_URL}`;
  }

  isAuthenticated(): Observable<boolean> {
    return this.http.get<UserInfos>(this.AUTH_STATUS_API_URL, { withCredentials: true })
      .pipe(
        tap(userInfos => {
          if (userInfos) {
            this.accountService.setUserInfos(userInfos)
          } else {
            this.accountService.setIsDisconnected()
          }
        }),
        map(value => value !== null)
      );
  }

  logout(): void {
    this.accountService.setIsDisconnected();
    window.location.href = `${this.LOGOUT_API_URL}`;
  }
}
