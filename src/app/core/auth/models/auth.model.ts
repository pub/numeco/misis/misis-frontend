export interface CodeExchange {
    code: string;
    nonce: string;
}

export interface AuthorizationCodeTokens {
    idToken: string;
    accessToken: string;
    refreshToken: string;
    accessTokenExpiresIn: number;
    legalDocuments?:LegalDocuments;
}

export interface LegalDocuments {
    version: string;
    content: string;
}
