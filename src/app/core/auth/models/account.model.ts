export interface UserInfos {
    username: string;
    groups: string[];
    email: string;
}
export interface Account {
    user: UserInfos | null;
    status: 'CONNECTED' | 'DISCONNECTED' | 'LOADING';
}
