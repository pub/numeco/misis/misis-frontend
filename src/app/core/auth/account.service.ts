import {Injectable, signal} from "@angular/core";
import {Account, UserInfos} from "./models/account.model";

const BASE_ACCOUNT: Account = {
    user: null,
    status: 'DISCONNECTED'
};

@Injectable({
    providedIn: 'root'
})
export class AccountService {
    private readonly _account = signal<Account>(BASE_ACCOUNT);
    readonly account = this._account.asReadonly();


    setIsConnecting(): void {
        this._account.update(account => ({...account, status: 'LOADING'}));
    }

    setUserInfos(user: UserInfos): void {
        this._account.set({user, status: 'CONNECTED'});
    }

    setIsDisconnected(): void {
        this._account.update(account => ({user: null, status: 'DISCONNECTED'}));
    }
}
