import {Component} from '@angular/core';
import {RouterOutlet} from "@angular/router";

@Component({
    selector: 'misis-main',
    standalone: true,
  imports: [
    RouterOutlet,
  ],
    template: `
      <router-outlet/>
    `,
})
export class MainComponent {
}
