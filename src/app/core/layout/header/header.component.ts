import { Component, HostListener, inject, OnInit } from '@angular/core';
import { RouterLink } from "@angular/router";
import { QuickAccessAuthenticatedComponent } from "./quick-access/quick-access-authenticated.component";
import { QuickAccessAnonymousComponent } from "./quick-access/quick-access-anonymous.component";
import { AuthenticationService } from '../../auth/authentication.service';
import { AccountService } from '../../auth/account.service';
import { debounceTime, Subject } from 'rxjs';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'misis-header',
  standalone: true,
  imports: [
    RouterLink,
    QuickAccessAuthenticatedComponent,
    QuickAccessAnonymousComponent,
  ],
  template: `
    <header role="banner" class="fr-header">
      <div class="fr-header__body">
        <div class="fr-container">
          <div class="fr-header__body-row">
            <div class="fr-header__brand fr-enlarge-link">
              <div class="fr-header__brand-top">
                <div class="fr-header__logo">
                  <p class="fr-logo" [innerHTML]="wording.LOGO_TITLE"></p>
                </div>
                <div class="fr-header__navbar">
                  <button data-fr-opened="false" aria-controls="header-menu" title="Menu" type="button" id="header-menu-btn" class="fr-btn--menu fr-btn">
                    <span id="text-menu">
                      Menu
                    </span>
                  </button>
                </div>
              </div>
              <div class="fr-header__service">
                <a routerLink="/accueil" [title]="'Accueil'+wording.LINK_TITLE">
                  <p class="fr-header__service-title">
                    MISIS
                  </p>
                </a>
                <p class="fr-header__service-tagline">
                  Outil de Mesure d'Impact des Sites Internet et leur Suivi
                </p>
              </div>
          </div>
          <div class="fr-header__tools">
            <div class="fr-header__tools-links">
                @if (isDSFRDesktop) {
                  @if (accountService.account().status === 'CONNECTED') {
                    <ul misis-authenticated-quick-access class="fr-btns-group"></ul>
                  } @else {
                    <ul misis-anonymous-quick-access class="fr-btns-group"></ul>
                  }
		}
            </div>
          </div>
        </div>
      </div>
      <div class="fr-header__menu fr-modal" id="modal-burger" aria-labelledby="button-burger" role="dialog">
        <div class="fr-container">
          <button class="fr-btn--close fr-btn" aria-controls="modal-burger" title="Fermer">
            Fermer
          </button>
          <div class="fr-header__menu-links">
            @if (!isDSFRDesktop) {
              @if (accountService.account().status === 'CONNECTED') {
                <ul misis-authenticated-quick-access notificationTitle="Mes notifications" class="fr-btns-group"></ul>
              } @else {
                <ul misis-anonymous-quick-access  class="fr-btns-group"></ul>
              }
	    }
          </div>
        </div>
      </div>
      </div>
    </header>`,
  styles: ``
})
export class HeaderComponent implements OnInit {

  accountService = inject(AccountService);
  authenticationService = inject(AuthenticationService);
  wording = environment.WORDING;

  DSFR_BREAKPOINT_LG = 992;
  isDSFRDesktop = window.innerWidth > this.DSFR_BREAKPOINT_LG;
  private resizeSubject = new Subject<void>();

  constructor() {
    this.resizeSubject.pipe(
      debounceTime(200)
    ).subscribe(() => {
      this.getScreenSize();
    })
  }

  ngOnInit(): void {
    this.authenticationService.isAuthenticated();
  }

  @HostListener('window:resize')
  onResize() {
    this.resizeSubject.next();
  }

  private getScreenSize() {
    this.isDSFRDesktop = window.innerWidth > this.DSFR_BREAKPOINT_LG;
  }
}

