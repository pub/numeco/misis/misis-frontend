import {Component} from '@angular/core';
import {RouterLink} from "@angular/router";

@Component({
  selector: 'ul[misis-anonymous-quick-access]',
  standalone: true,
  imports: [
    RouterLink
  ],
  template: `
    <li>
      <a routerLink="/login" class="fr-btn fr-icon-lock-line">
        Se connecter
      </a>
    </li>
  `,
  styles: ``
})
export class QuickAccessAnonymousComponent {

}
