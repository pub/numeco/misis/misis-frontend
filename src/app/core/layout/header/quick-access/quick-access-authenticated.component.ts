import {Component, inject} from '@angular/core';
import {AsyncPipe} from "@angular/common";
import {RouterLink} from "@angular/router";
import {NotificationService} from "../../../../views/protected/notification/notification.service";
import {AuthenticationService} from "../../../auth/authentication.service";
import { environment } from '../../../../../environments/environment';
@Component({
  selector: 'ul[misis-authenticated-quick-access]',
  standalone: true,
  imports: [
    AsyncPipe,
    RouterLink
  ],
  template: `
    @if (notifications | async; as notif) {
      <li>
        <a routerLink="/notifications"
           [class.fr-icon-notification-3-line]="notif.length === 0"
           [class.fr-icon-notification-3-fill]="notif.length"
           class="fr-btn"
           [title]="'Notifications'+wording.LINK_TITLE">
            Mes notifications
        </a>
      </li>
    } @else {
      <li>
        <a routerLink="/notifications"
           class="fr-btn fr-icon-notification-3-line"
           [title]="'Notifications'+wording.LINK_TITLE">
            Mes notifications
        </a>
      </li>
    }
    <li>
      <a class="fr-btn fr-icon-lock-line" (click)="logout()"
         href="javascript:void(0)">
        Se déconnecter
      </a>
    </li>
  `,
  styles: ``
})
export class QuickAccessAuthenticatedComponent {
  wording = environment.WORDING;
  private readonly notificationService = inject(NotificationService);
  private readonly authenticationService = inject(AuthenticationService);

  notifications = this.notificationService.notifications$;

  logout() {
    this.authenticationService.logout();
  }

}
