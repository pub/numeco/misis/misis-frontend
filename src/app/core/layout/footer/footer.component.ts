import {Component} from '@angular/core';
import {DisplayDialogComponent} from "../display-dialog/display-dialog.component";
import {RouterLink} from "@angular/router";
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'misis-footer',
  standalone: true,
  imports: [
    DisplayDialogComponent,
    RouterLink
  ],
  template: `
    <footer class="fr-footer fr-mt-1v" role="contentinfo">
      <div class="fr-container">
        <div class="fr-footer__body">
          <div class="fr-footer__brand fr-enlarge-link">
            <a href="/" [title]="'Retour à l’accueil du site'+wording.LINK_TITLE">
              <p class="fr-logo" [innerHTML]="wording.LOGO_TITLE"></p>
            </a>
          </div>
          <div class="fr-footer__content">
            <ul class="fr-footer__content-list">
              <li class="fr-footer__content-item"><a class="fr-footer__content-link" target="_blank"
                                                     rel="noopener external"
                                                     title="legifrance.gouv.fr - nouvelle fenêtre"
                                                     href="https://legifrance.gouv.fr">legifrance.gouv.fr</a>
              </li>
              <li class="fr-footer__content-item">
                <a class="fr-footer__content-link" target="_blank" rel="noopener external" title="info.gouv.fr - nouvelle fenêtre" href="https://www.info.gouv.fr/">
                  info.gouv.fr
                </a>
              </li>
              <li class="fr-footer__content-item"><a class="fr-footer__content-link" target="_blank"
                                                     rel="noopener external"
                                                     title="service-public.fr - nouvelle fenêtre"
                                                     href="https://service-public.fr">service-public.fr</a>
              </li>
              <li class="fr-footer__content-item"><a class="fr-footer__content-link" target="_blank"
                                                     rel="noopener external"
                                                     title="data.gouv.fr - nouvelle fenêtre"
                                                     href="https://data.gouv.fr">data.gouv.fr</a></li>
            </ul>
          </div>
        </div>
        <div class="fr-footer__bottom">
          <ul class="fr-footer__bottom-list">
            <li class="fr-footer__bottom-item">
              <a class="fr-footer__bottom-link" [routerLink]="['plan-du-site']">
                Plan du site
              </a>
            </li>
            <li class="fr-footer__bottom-item">
              <a class="fr-footer__bottom-link" [routerLink]="['accessibilite']">
                Accessibilité : non conforme
              </a>
            </li>
            <li class="fr-footer__bottom-item">
              <a class="fr-footer__bottom-link" [routerLink]="['mentions-legales']">
                Mentions légales
              </a>
            </li>
            <li class="fr-footer__bottom-item">
              <a class="fr-footer__bottom-link" [routerLink]="['donnees-personnelles']">
                Données personnelles et cookies
              </a>
            </li>
            <li class="fr-footer__bottom-item">
              <button class="fr-footer__bottom-link" data-fr-opened="false" aria-controls="fr-consent-modal">
                  Gestion des cookies
              </button>
            </li>
            <li class="fr-footer__bottom-item">
              <button class="fr-footer__bottom-link fr-icon-theme-fill fr-btn--icon-left" aria-controls="fr-theme-modal" data-fr-opened="false">
                Paramètres d'affichage
              </button>
            </li>
          </ul>
          <div class="fr-footer__bottom-copy">
            <p>
              Sauf mention explicite de propriété intellectuelle détenue par des tiers, les contenus de ce site sont proposés sous
              <a href="https://github.com/etalab/licence-ouverte/blob/master/LO.md" target="_blank" rel="noopener external" title="licence etalab-2.0 - nouvelle fenêtre">
                licence etalab-2.0
              </a>
            </p>
          </div>
        </div>
      </div>
    </footer>

    <misis-display-dialog/>
  `,
  styles: [`

  `]
})
export class FooterComponent {
  wording = environment.WORDING;
}
