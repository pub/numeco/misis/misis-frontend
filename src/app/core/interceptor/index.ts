import {errorInterceptor} from "./error.interceptor";

export const httpInterceptorProviders = [
    errorInterceptor,
]
