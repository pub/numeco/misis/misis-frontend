import {HttpErrorResponse, HttpInterceptorFn} from '@angular/common/http';
import {throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {AuthenticationService} from "../auth/authentication.service";
import {inject} from "@angular/core";

export const errorInterceptor: HttpInterceptorFn = (req, next) => {
    const authService = inject(AuthenticationService);
    return next(req).pipe(
        catchError((error: HttpErrorResponse) => {
            switch (error.status) {
                case 401 :
                    handle401(error, authService);
                    break;
                case 404 :
                    window.location.href = '/page-non-trouvee';
                    break;
                case 500 :
                    window.location.href = '/erreur-inattendue';
                    break;
            }
            return throwError(() => error);
        })
    );
};

export function handle401(err: HttpErrorResponse, authService: AuthenticationService) {
    if (err.status === 401) {
        authService.login();
    }
    return throwError(() => err);
}
