import {Injectable} from "@angular/core";
import {RouterStateSnapshot, TitleStrategy} from "@angular/router";
import { environment } from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class MisisTitleStrategy extends TitleStrategy {
    override updateTitle(routerState: RouterStateSnapshot) {
        const title = this.buildTitle(routerState);
        if (title) {
            document.title = `${title}${environment.WORDING.LINK_TITLE}`;
        }
    }
}
