import {CanActivateFn, Router} from "@angular/router";
import {inject} from "@angular/core";
import {AuthenticationService} from "../auth/authentication.service";
import {map, tap} from "rxjs";

export const homepageGuard: CanActivateFn = (route, state) => {
  const router = inject(Router);
  const authenticationService = inject(AuthenticationService);

  return authenticationService.isAuthenticated()
    .pipe(
      tap(value => {
        if (route.url.length === 0 && value) {
          router.navigate(['tableaux-de-suivi']);
        }
      }),
      map(value => !value),
    );
}
