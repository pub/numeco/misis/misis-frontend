import {CanActivateFn} from '@angular/router';
import {inject} from "@angular/core";
import {AuthenticationService} from "../auth/authentication.service";
import { map } from 'rxjs';

export const authGuard: CanActivateFn = (route, state) => {
    const authenticationService = inject(AuthenticationService);

    return authenticationService.isAuthenticated();
};

export const anonymousGuard: CanActivateFn = (route, state) => {
  const authenticationService = inject(AuthenticationService);

  return authenticationService.isAuthenticated().pipe(
    map(isAuthenticated => !isAuthenticated)
  );
};
