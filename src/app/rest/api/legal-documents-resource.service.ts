import {Inject, Injectable, Optional} from "@angular/core";
import {HttpClient, HttpEvent, HttpHeaders, HttpResponse} from "@angular/common/http";
import {Configuration} from "../configuration";
import {BASE_PATH} from "../variables";
import {SettingsFormDto} from "../model/settings-form-dto";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class LegalDocumentsResourceService {

  protected basePath = '/';
  public defaultHeaders = new HttpHeaders();
  public configuration = new Configuration({withCredentials: true});

  constructor(protected httpClient: HttpClient, @Optional()@Inject(BASE_PATH) basePath: string, @Optional() configuration: Configuration) {
    if (basePath) {
      this.basePath = basePath;
    }
    if (configuration) {
      this.configuration = configuration;
      this.basePath = basePath || configuration.basePath || this.basePath;
    }
  }

  public legalDocumentAcceptCguPost(body?: SettingsFormDto, observe?: 'body', reportProgress?: boolean): Observable<any>;
  public legalDocumentAcceptCguPost(body?: SettingsFormDto, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
  public legalDocumentAcceptCguPost(body?: SettingsFormDto, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
  public legalDocumentAcceptCguPost(body?: SettingsFormDto, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {


    let headers = this.defaultHeaders;

    // authentication (SecurityScheme) required
    // to determine the Accept header
    let httpHeaderAccepts: string[] = [
    ];
    const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
    if (httpHeaderAcceptSelected != undefined) {
      headers = headers.set('Accept', httpHeaderAcceptSelected);
    }

    // to determine the Content-Type header
    const consumes: string[] = [
      'application/json'
    ];
    const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(consumes);
    if (httpContentTypeSelected != undefined) {
      headers = headers.set('Content-Type', httpContentTypeSelected);
    }

    return this.httpClient.request<any>('post',`${this.basePath}/legalDocuments`,
      {
        body: body,
        withCredentials: this.configuration.withCredentials,
        headers: headers,
        observe: observe,
        reportProgress: reportProgress
      }
    );
  }

}
