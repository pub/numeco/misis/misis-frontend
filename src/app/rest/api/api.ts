export * from './analyser-resource.service';
import {AnalyserResourceService} from './analyser-resource.service';
import {AuthenticateResourceService} from './authenticate-resource.service';
import {LogoutResourceService} from './logout-resource.service';
import {StatisticsResourceService} from './statistics-resource.service';
import {WebsiteDetailsResourceService} from './website-details-resource.service';
import {WebsiteResourceService} from './website-resource.service';
import {WebsiteSettingsRessourceService} from './website-settings-ressource.service';

export * from './authenticate-resource.service';
export * from './logout-resource.service';
export * from './statistics-resource.service';
export * from './website-details-resource.service';
export * from './website-resource.service';
export * from './website-settings-ressource.service';
export const APIS = [AnalyserResourceService, AuthenticateResourceService, LogoutResourceService, StatisticsResourceService, WebsiteDetailsResourceService, WebsiteResourceService, WebsiteSettingsRessourceService];
