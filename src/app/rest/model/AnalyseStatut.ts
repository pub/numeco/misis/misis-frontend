export type AnalyseStatut = 'NOUVEAU' | 'EN_COURS' | 'TERMINEE' | 'INVALIDE' | 'ERREUR_URL_VIDE' | 'ERREUR_URL_MALFORMEE' | 'ERREUR_URL_NON_RESOLUE' | 'ERREUR_PAGE_302' | 'ERREUR_PAGE_404' | 'ERREUR_PAGE_500';

export const AnalyseStatut = {
  NOUVEAU: 'NOUVEAU' as AnalyseStatut,
  EN_COURS: 'EN_COURS' as AnalyseStatut,
  TERMINEE: 'TERMINEE' as AnalyseStatut,
  INVALIDE: 'INVALIDE' as AnalyseStatut,
  ERREUR_URL_VIDE: 'ERREUR_URL_VIDE' as AnalyseStatut,
  ERREUR_URL_MALFORMEE: 'ERREUR_URL_MALFORMEE' as AnalyseStatut,
  ERREUR_URL_NON_RESOLUE: 'ERREUR_URL_NON_RESOLUE' as AnalyseStatut,
  ERREUR_PAGE_302: 'ERREUR_PAGE_302' as AnalyseStatut,
  ERREUR_PAGE_404: 'ERREUR_PAGE_404' as AnalyseStatut,
  ERREUR_PAGE_500: 'ERREUR_PAGE_500' as AnalyseStatut,

};
