export type Alert = 'WARNING' | 'ERROR';

export const Alert = {
  WARNING: 'WARNING' as Alert,
  ERROR: 'ERROR' as Alert,
};
