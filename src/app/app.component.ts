import {Component, inject} from '@angular/core';
import {HeaderComponent} from "./core/layout/header/header.component";
import {MainComponent} from "./core/layout/main/main.component";
import {FooterComponent} from "./core/layout/footer/footer.component";
import {AuthenticationService} from "./core/auth/authentication.service";
import {ActivatedRoute, Params} from "@angular/router";
import {ModalComponent} from "./shared/components/modal/modal.component";
import {Location} from "@angular/common";
import {DomSanitizer, SafeHtml} from "@angular/platform-browser";
import {LegalDocumentsResourceService} from "./rest/api/legal-documents-resource.service";
import {SkipLinksComponent} from "./shared/components/skip-links/skip-links.component";
import {BreadcrumbComponent} from "./shared/components/breadcrumb/breadcrumb.component";
import { CookiesManagerComponent } from "./shared/components/cookies-manager/cookies-manager.component";

@Component({
    selector: 'app-root',
    standalone: true,
  imports: [
    HeaderComponent,
    MainComponent,
    FooterComponent,
    ModalComponent,
    SkipLinksComponent,
    BreadcrumbComponent,
    CookiesManagerComponent
],
    template: `
      <misis-cookies-manager/>
      <misis-skip-links/>
      <misis-header id="header"/>
      <misis-breadcrumb id="breadcrumb"/>
      <misis-main id="main-content"/>
      <misis-footer id="footer"/>
      @if (documentContent) {
        <misis-modal id="fr-modal-cgu"
                     modalTitle="Conditions Générales d’Utilisation (CGU)"
                     modalClosingLabelInHeader="Fermer"
                     primaryLabel="Accepter et continuer"
                     [primaryAction]="primaryAction"
                     secondaryLabel="Refuser et se déconnecter"
                     [secondaryAction]="secondaryAction"
                     [isOpenModal]="true"
        >
          <span [innerHTML]="documentContent"></span>
        </misis-modal>
      }
    `,
    styles: [`
      :host {
        height: 100%;
        display: flex;
        flex-direction: column;
      }

      misis-main {
        flex-grow: 1;
      }
    `],
})
export class AppComponent {

    /** Dependencies **/
    private readonly authenticationService = inject(AuthenticationService);
    private readonly route = inject(ActivatedRoute);
    private readonly location = inject(Location);
    private sanitizer = inject(DomSanitizer);
    private readonly legalDocumentsResourceService = inject(LegalDocumentsResourceService);
    documentContent:SafeHtml|null;

    constructor() {
        this.route.queryParams.subscribe(this.onPathChange);
    }

    onPathChange = (params: Params) => {
      const content = localStorage.getItem("cgu");
      if (content) {
        this.documentContent = this.sanitizer.bypassSecurityTrustHtml(content);
      }
    }

    primaryAction = () => {
      this.legalDocumentsResourceService.legalDocumentAcceptCguPost().subscribe(
        response => {
          localStorage.removeItem("cgu");
          this.documentContent=null;
        }
      );
    }

    secondaryAction = () => {
        this.authenticationService.logout();
        this.location.replaceState(window.location.pathname);
        localStorage.removeItem("cgu");
    }
}
