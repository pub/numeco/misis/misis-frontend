import {ApplicationConfig} from '@angular/core';
import {provideRouter, TitleStrategy, withComponentInputBinding} from '@angular/router';

import {routes} from './app.routes';
import {provideHttpClient, withInterceptors} from "@angular/common/http";
import {httpInterceptorProviders} from "./core/interceptor";
import {MisisTitleStrategy} from "./core/service/misis-title-strategy.service";
import {environment} from "../environments/environment";
import {BASE_PATH} from "./rest/variables";

import {provideCharts, withDefaultRegisterables} from "ng2-charts";
import moment from 'moment';

moment.locale('fr');

export const appConfig: ApplicationConfig = {
    providers: [
        provideRouter(routes, withComponentInputBinding()),
        provideHttpClient(withInterceptors(httpInterceptorProviders)),
        {provide: TitleStrategy, useClass: MisisTitleStrategy},
        {provide: BASE_PATH, useValue: environment.BASE_API},
        provideCharts(withDefaultRegisterables()),
    ]
};


