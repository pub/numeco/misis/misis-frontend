import {Routes} from '@angular/router';
import {anonymousGuard, authGuard} from "./core/guard/auth.guard";
import {homepageGuard} from "./core/guard/homepage.guard";
import {generateSkipLinks} from "./shared/components/skip-links/skip-links-configuration";

export const routes: Routes = [
  {
    path: 'accueil',
    redirectTo: ''
  },
  {
    path: '',
    data: {
      title: 'Accueil',
    },
    title: 'Accueil',
    children: [
      {
        path: '',
        loadComponent: () => import('././views/public/home/home.component'),
        title: 'Accueil',
        canActivate:[homepageGuard],
        data: {
          skiplinks: generateSkipLinks()
        }
      },
      {
        path: 'notifications',
        loadComponent: () => import('././views/protected/notification/notification.component'),
        title: 'Vos notifications',
        data: {
          title: 'Vos notifications',
          skiplinks: generateSkipLinks(),
        },
        canMatch: [authGuard],
      },
      {
        path: 'creation-suivi-de-site',
        loadComponent: () => import('././views/protected/suivis/formulaire-creation-suivi/creation-suivi-site.component'),
        title: 'Création de suivi de site',
        data: {
          title: 'Création de suivi de site',
          skiplinks: generateSkipLinks(),
        },
        canMatch: [authGuard],
      },
      {
        path: 'login',
        loadComponent: () => import('./views/public/home/login-page.component'),
        title: 'Connexion',
        data: { title: 'Connexion' },
        canMatch:[anonymousGuard],
      },
      {
        path: 'tableaux-de-suivi',
        title: 'Vos suivis',
        data: {
          title: 'Vos suivis',
          skiplinks: generateSkipLinks(),
        },
        canMatch: [authGuard],
        children: [
          {
            path:'',
            loadComponent: () => import('././views/protected/suivis/suivis-wrapper.component'),
          },
          {
            path: 'detail/:id',
            title: 'Détail du suivi',
            data: {
              title: 'Détail du suivi',
              skiplinks: generateSkipLinks(),
            },
            loadComponent: () => import('./views/protected/suivis/detail-suivi/detail-suivi.component'),
            children: [
              {
                path: '',
                pathMatch: "full",
                loadComponent: () => import('././views/protected/suivis/detail-suivi/main-content/liste-des-pages/liste-des-pages.component'),
              },
              {
                path: 'groupe/:id',
                redirectTo: 'groupe/:id/liste-des-pages',
              },
              {
                path: 'groupe/:id/liste-des-pages',
                loadComponent: () => import('././views/protected/suivis/detail-suivi/main-content/liste-des-pages/liste-des-pages.component'),
                data: {
                  title: 'Liste des pages',
                  skiplinks: generateSkipLinks(),
                },
              },
              {
                path: 'groupe/:id/images',
                loadComponent: () => import('././views/protected/suivis/detail-suivi/main-content/images/images.component'),
                data: {
                  title: 'Images',
                  skiplinks: generateSkipLinks(),
                },
              },
              {
                path: 'groupe/:id/documents-pdf',
                loadComponent: () => import('././views/protected/suivis/detail-suivi/main-content/documents-pdf/documents-pdf.component'),
                data: {
                  title: 'Documents PDF',
                  skiplinks: generateSkipLinks(),
                },
              },
              {
                path: 'groupe/:id/autres-documents',
                loadComponent: () => import('././views/protected/suivis/detail-suivi/main-content/other-documents/other-documents.component'),
                data: {
                  title: 'Autres documents',
                  skiplinks: generateSkipLinks(),
                },
              },
              {
                path: 'groupe/:id/diffusion',
                loadComponent: () => import('././views/protected/suivis/detail-suivi/main-content/streaming/streaming.component'),
                data: {
                  title: 'Médias',
                  skiplinks: generateSkipLinks(),
                },
              },
              {
                path: 'groupe/:id/donnees-transferees',
                loadComponent: () => import('././views/protected/suivis/detail-suivi/main-content/donnees-transferees/donnees-transferees.component'),
                data: {
                  title: 'Données Transférées',
                  skiplinks: generateSkipLinks(),
                },
              },
              {
                path: 'groupe/:id/requetes',
                loadComponent: () => import('././views/protected/suivis/detail-suivi/main-content/requetes/requetes.component'),
                data: {
                  title: 'Nombre de requêtes',
                  skiplinks: generateSkipLinks(),
                },
              },
              {
                path: 'groupe/:id/elements-dom',
                loadComponent: () => import('././views/protected/suivis/detail-suivi/main-content/elements-dom/elements-dom.component'),
                data: {
                  title: 'Éléments du DOM',
                  skiplinks: generateSkipLinks(),
                },
              },
              {
                path: 'parametrer',
                loadComponent: () => import('././views/protected/suivis/detail-suivi/parametrer-un-suivi/parametrer-un-suivi.component'),
                title: 'Paramètres',
                data: {
                  title: 'Paramètres',
                  skiplinks: generateSkipLinks(),
                },
              },
            ]
          },
        ]
      },
      {
        path: 'plan-du-site',
        loadComponent: () => import('./views/public/home/plan-du-site.component'),
        title: 'Plan du site',
        data:{
          title: 'Plan du site',
          skiplinks: generateSkipLinks(),
        }
      },
      {
        path: 'accessibilite',
        loadComponent: () => import('./views/public/home/accessibilite.component'),
        title: 'Accessibilité',
        data:{
          title: 'Accessibilité',
          skiplinks: generateSkipLinks(),
        }
      },
      {
        path: 'mentions-legales',
        loadComponent: () => import('./views/public/home/mentions-legales.component'),
        title: 'Mentions légales',
        data:{
          title: 'Mentions légales',
          skiplinks: generateSkipLinks(),
        }
      },
      {
        path: 'donnees-personnelles',
        loadComponent: () => import('./views/public/home/donnees-personnelles.component'),
        title: 'Données personnelles',
        data:{
          title: 'Données personnelles',
          skiplinks: generateSkipLinks(),
        }
      },
      {
        path: 'erreur-inattendue',
        pathMatch: 'full',
        loadComponent: () => import('./views/public/unexpected-error/unexpected-error.component'),
        title: 'Erreur inattendue',
        data: {
          title: 'Erreur inattendue',
          skiplinks: generateSkipLinks(),
        },
      },
      {
        path: 'forbidden',
        pathMatch: 'full',
        loadComponent: () => import('./views/public/forbidden/forbidden.component'),
        title: 'Accès interdit',
        data: {
          title: 'Accès interdit',
          skiplinks: generateSkipLinks(),
        },
      },
      {
        path: '**',
        pathMatch: 'full',
        loadComponent: () => import('./views/public/not-found/not-found.component'),
        title: 'Page non trouvée',
        data: {
          title: 'Page non trouvée',
          skiplinks: generateSkipLinks(),
        },
      },
    ]
  },
];
